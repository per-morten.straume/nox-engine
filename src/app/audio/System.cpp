/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/audio/Buffer.h>
#include <nox/app/audio/System.h>

#include <cassert>

namespace nox { namespace app
{

namespace audio
{

System::System()
{
}


System::~System()
{
}

void System::stopAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->stopAudio();
	}
}

void System::pauseAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->pauseAudio();
	}
}

void System::resumeAllSounds()
{
	for (const auto& buffer : this->buffers)
	{
		buffer->resumeAudio();
	}
}

void System::shutdown()
{
	while (this->buffers.empty() == false)
	{
		this->buffers.front()->stopAudio();
		this->buffers.pop_front();
	}
}

Buffer* System::createAudioBuffer(const std::shared_ptr<resource::Handle>& handle)
{
	std::unique_ptr<Buffer> buffer = this->createBuffer();
	buffer->initialize(handle);

	Buffer* bufferPointer = buffer.get();
	this->buffers.push_front(std::move(buffer));

	return bufferPointer;
}

void System::releaseAudioBuffer(Buffer* audioBuffer)
{
	assert(audioBuffer != nullptr);

	audioBuffer->stopAudio();

	this->buffers.remove_if(
			[audioBuffer](const std::unique_ptr<Buffer>& listBuffer)
			{
				return (listBuffer.get() == audioBuffer);
			}
	);
}

}
} }
