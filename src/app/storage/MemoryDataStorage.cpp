/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/storage/MemoryDataStorage.h>

#include <nox/log/Logger.h>

#include <unordered_map>
#include <istream>
#include <ostream>
#include <sstream>

namespace nox { namespace app { namespace storage {

using FileMap = std::unordered_map<std::string, std::unique_ptr<std::stringbuf>>;

class MemoryDataStorage::Impl
{
public:
	FileMap files;
};

MemoryDataStorage::MemoryDataStorage():
	d(std::make_unique<Impl>())
{
}

MemoryDataStorage::~MemoryDataStorage() = default;

bool MemoryDataStorage::fileExists(const std::string& filePath) const
{
	return this->d->files.count(filePath) > 0;
}

std::unique_ptr<std::istream> MemoryDataStorage::openReadableFile(const std::string& filePath)
{
	auto& buffer = this->d->files[filePath];
	if (buffer != nullptr)
	{
		return std::make_unique<std::istream>(buffer.get());
	}
	else
	{
		return nullptr;
	}
}

std::unique_ptr<std::ostream> MemoryDataStorage::openWritableFile(const std::string& filePath, bool append)
{
	auto& buffer = this->d->files[filePath];
	if (buffer == nullptr)
	{
		buffer = std::make_unique<std::stringbuf>();
	}

	if (append == false)
	{
		buffer->str("");
	}

	return std::make_unique<std::ostream>(buffer.get());
}

std::string MemoryDataStorage::readFileContent(const std::string& filePath)
{
	auto stream = this->openReadableFile(filePath);

	if (stream == nullptr)
	{
		return std::string();
	}
	else
	{
		std::istreambuf_iterator<char> streamStart(*stream);
		std::istreambuf_iterator<char> streamEnd;

		return std::string(streamStart, streamEnd);
	}
}

void MemoryDataStorage::writeFileContent(const std::string& filePath, const std::string& content, bool append)
{
	auto stream = this->openWritableFile(filePath, append);

	*stream << content;
}

void MemoryDataStorage::removeFile(const std::string &filePath)
{
	this->d->files.erase(filePath);
}

} } }

