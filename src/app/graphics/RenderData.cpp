/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/RenderData.h>

namespace nox { namespace app
{
namespace graphics
{

RenderData::RenderData(GlContextState* glState, const TextureManager* textureManager):
	glState(glState),
	textureManager(textureManager)
{
}

RenderData::RenderData():
	RenderData(nullptr, nullptr)
{
}

#if NOX_OPENGL_SUPPORT_VAO
GLuint RenderData::bindVertexArray(GLuint vao)
{
	return this->glState->bindVertexArray(vao);
}
#endif

GLuint RenderData::bindBuffer(GLenum target, GLuint vbo)
{
	return this->glState->bindBuffer(target, vbo);
}

GLuint RenderData::bindShaderProgram(GLuint shaderProgram)
{
	return this->glState->bindShaderProgram(shaderProgram);
}

GLuint RenderData::bindShaderProgram(const GlslProgram& shaderProgram)
{
	return this->glState->bindShaderProgram(shaderProgram.getId());
}

GLuint RenderData::bindTexture(GLenum target, GLuint texture)
{
	return this->glState->bindTexture(target, texture);
}

void RenderData::setStencilFailOperation(GLenum operation)
{
	this->glState->setStencilFailOperation(operation);
}

void RenderData::setStencilMask(GLuint mask)
{
	this->glState->setStencilMask(mask);
}

void RenderData::setStencilFunc(GLenum func, GLint ref, GLuint mask)
{
	this->glState->setStencilFunc(func, ref, mask);
}

void RenderData::enable(GLenum state)
{
	this->glState->enable(state);
}

void RenderData::disable(GLenum state)
{
	this->glState->disable(state);
}

GlContextState* RenderData::getState()
{
	return this->glState;
}

GlVersion RenderData::getGlVersion() const
{
	return this->glState->getGlVersion();
}

const TextureManager* RenderData::getTextureManager() const
{
	return this->textureManager;
}

GLuint RenderData::getBoundVao() const
{
	return this->glState->getBoundVao();
}

GLuint RenderData::getBoundVbo(GLenum target) const
{
	return this->glState->getBoundVbo(target);
}

GLuint RenderData::getBoundShaderProgram() const
{
	return this->glState->getBoundShaderProgram();
}

GLuint RenderData::getBoundTexture(GLenum target) const
{
	return this->glState->getBoundTexture(target);
}

}
} }
