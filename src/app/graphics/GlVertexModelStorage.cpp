/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlVertexModelStorage.h>

namespace nox { namespace app { namespace graphics {

GlVertexModelStorage::~GlVertexModelStorage() = default;

GlPackedVertexAttributes* GlVertexModelStorage::createPackedVertexAttributes(const GLenum bufferUsage, const GlPackedVertexAttributeDef& packDef)
{
	auto attrib = std::make_unique<GlPackedVertexAttributes>(bufferUsage, packDef);
	auto attribPtr = attrib.get();

	this->packedAttributes.push_back(std::move(attrib));

	this->onAttributeCreated();

	return attribPtr;
}

void GlVertexModelStorage::bind(GlContextState* state)
{
	for (auto& attrib : this->packedAttributes)
	{
		attrib->bind(state);
	}

	if (this->elementArrayBuffer.isValid())
	{
		this->elementArrayBuffer.bind(state);
	}
}

void GlVertexModelStorage::enableElementArrayBuffer(const GLenum usage, GlContextState* state)
{
	if (this->elementArrayBuffer.isValid())
	{
		throw std::logic_error("Tried enabling already enabled element array buffer.");
	}
	else
	{
		this->elementArrayBuffer = GlBuffer::generateElementArrayBuffer(usage);
		this->onElementArrayBufferEnabled(state);
	}
}

GlBuffer& GlVertexModelStorage::getElementArrayBuffer()
{
	return this->elementArrayBuffer;
}

void GlVertexModelStorage::onElementArrayBufferEnabled(GlContextState*)
{
}

void GlVertexModelStorage::onAttributeCreated()
{
}

} } }
