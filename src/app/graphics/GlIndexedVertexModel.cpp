/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/GlIndexedVertexModel.h>

#include <fmt/format.h>

namespace nox { namespace app { namespace graphics {

GlIndexedVertexModel::GlIndexedVertexModel(GlContextState* glState, std::unique_ptr<GlVertexModelStorage> modelStorage, const GLenum elementArrayBufferUsage, const GlDataType elementDataType):
	GlVertexModel(glState, std::move(modelStorage)),
	elementDataType(elementDataType)
{
	this->getStorage()->enableElementArrayBuffer(elementArrayBufferUsage, glState);
}

GlIndexedVertexModel::GlIndexedVertexModel(GlContextState* glState, const GLenum elementArrayBufferUsage, const GlDataType elementDataType):
	GlVertexModel(glState),
	elementDataType(elementDataType)
{
	this->getStorage()->enableElementArrayBuffer(elementArrayBufferUsage, glState);
}

GlIndexedVertexModel::GlIndexedVertexModel():
	GlVertexModel(nullptr, nullptr),
	elementDataType(GlDataType::UNSIGNED_SHORT)
{
}


GlIndexedVertexModel::~GlIndexedVertexModel() = default;

GlIndexedVertexModel::GlIndexedVertexModel(GlIndexedVertexModel&& other):
	GlVertexModel(std::move(other)),
	elementDataType(other.elementDataType)
{
}

GlIndexedVertexModel& GlIndexedVertexModel::operator=(GlIndexedVertexModel&& other)
{
	this->GlVertexModel::operator=(std::move(other));
	this->elementDataType = other.elementDataType;

	return *this;
}

void GlIndexedVertexModel::drawRange(const GLenum drawMode, const std::size_t firstVertex, const std::size_t numVertices)
{
	if (firstVertex + numVertices > this->getNumVertices())
	{
		auto message = fmt::format("Vertex draw range [{}, {}) is out of bounds. Num vertices is {}. Remember to call GlVertexModel::setNumVertices().", firstVertex, firstVertex + numVertices, this->getNumVertices());
		throw std::out_of_range(message);
	}

	if (numVertices > 0)
	{
		this->bind();

		const auto* startPtr = static_cast<const char*>(0) + (firstVertex * glDataTypeBytes(this->elementDataType));
		glDrawElements(drawMode, static_cast<GLsizei>(numVertices), glDataTypeToEnum(this->elementDataType), reinterpret_cast<const GLvoid*>(startPtr));
	}
}

GlBuffer& GlIndexedVertexModel::getElementArrayBuffer()
{
	return this->getStorage()->getElementArrayBuffer();
}

} } }
