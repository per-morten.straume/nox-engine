/*
 * NOX Engine
 *
 * Copyright (c) 2015-2016 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/actor/Actor.h>

#include <algorithm>
#include <sstream>

namespace nox { namespace logic { namespace actor
{

Actor::Actor(Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom, log::Logger logger):
	log(std::move(logger)),
	parentActor(nullptr),
	name(name),
	definitionName(definitionName),
	extendedFrom(extendedFrom),
	id(id),
	trivial(false),
	created(false),
	active(false),
	forcedActiveState(ForcedActiveState::NONE)
{
	std::ostringstream logNameStream;
	logNameStream << "Actor " << this->id << " '" << this->name << "'";

	this->log.setName(logNameStream.str());
}

Actor::Actor(Identifier id, const std::string& name, const std::string& definitionName, const std::vector<std::string>& extendedFrom):
	Actor(id, name, definitionName, extendedFrom, log::Logger())
{
}


Actor::Actor(Identifier id, const std::string& name, log::Logger logger):
	Actor(id, name, std::string(), {}, std::move(logger))
{
}

Actor::Actor(Identifier id, const std::string& name):
	Actor(id, name, log::Logger())
{
}

bool Actor::addComponent(std::unique_ptr<Component> component)
{
	std::vector<std::string> names = component->findInheritedNames();
	names.push_back(component->getName());

	const bool duplicate = std::any_of(
		names.begin(),
		names.end(),
		[this](std::string& name)
		{
			return (this->componentMap.find(name) != this->componentMap.end());
		}
	);

	if (duplicate == true)
	{
		return false;
	}
	else
	{
		for (std::string& name : names)
		{
			this->componentMap[name] = component.get();
		}

		component->setOwner(this);

		for (std::unique_ptr<Component>& attachedComponent : this->components)
		{
			attachedComponent->onComponentAttached(component.get());
			component->onComponentAttached(attachedComponent.get());
		}

		if (this->isCreated() == true)
		{
			component->onCreate();
		}

		if (this->isActive() == true)
		{
			component->onActivate();
		}

		this->components.push_back(std::move(component));

		return true;
	}
}

bool Actor::attachChildActor(std::unique_ptr<Actor> actor, const std::string& actorName)
{
	const auto childIt = this->childActorMap.find(actorName);

	if (childIt != this->childActorMap.end())
	{
		this->log.error().format("Tried to attach child actor with name \"%s\" that already is mapped.", actorName.c_str());
		return false;
	}

	this->childActorMap[actorName] = actor.get();

	actor->parentActor = this;

	if (this->created == true && actor->created == false)
	{
		actor->create();
	}
	else if (this->created == false && actor->created == true)
	{
		actor->destroy();
	}

	if (this->forcedActiveState != actor->forcedActiveState)
	{
		actor->forceActiveState(this->forcedActiveState);
	}

	for (const auto& component : this->components)
	{
		component->onChildActorAttached(actor.get(), actorName);
	}

	const std::vector<Child> grandChildren = actor->findChildrenRecursively(actorName);
	for (const Child& child : grandChildren)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorAttached(child.actor, child.name);
		}
	}

	this->childActors.push_back(std::move(actor));

	return true;
}

std::unique_ptr<Actor> Actor::detachChildActor(const std::string& actorName)
{
	const auto childIt = this->childActorMap.find(actorName);

	if (childIt == this->childActorMap.end())
	{
		this->log.error().format("Tried to detach child actor with name \"%s\" that is not mapped.", actorName.c_str());
		return nullptr;
	}

	const Actor* actorPointer = childIt->second;
	const std::string childName = childIt->first;

	this->childActorMap.erase(childIt);

	auto actorIt = std::find_if(
			this->childActors.begin(),
			this->childActors.end(),
			[actorPointer] (const std::unique_ptr<Actor>& actor)
			{
				return (actor.get() == actorPointer);
			}
	);

	if (actorIt == this->childActors.end())
	{
		this->log.error().format("Tried detaching child actor \"%s\" that was mapped, but not stored.", actorName.c_str());
		return nullptr;
	}

	for (const auto& component : this->components)
	{
		component->onChildActorDetached(actorIt->get(), actorName);
	}

	const std::vector<Child> grandChildren = (*actorIt)->findChildrenRecursively(childName);
	for (const Child& actor : grandChildren)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorDetached(actor.actor, actor.name);
		}
	}

	std::unique_ptr<Actor> actor = std::move(*actorIt);
	actor->parentActor = nullptr;

	this->childActors.erase(actorIt);

	return actor;
}

std::vector<std::unique_ptr<Actor>> Actor::detachAllChildActors()
{
	for (const auto& childPair : this->childActorMap)
	{
		for (const auto& component : this->components)
		{
			component->onChildActorDetached(childPair.second, childPair.first);
		}

		const std::vector<Child> grandChildren = childPair.second->findChildrenRecursively(childPair.first);
		for (const Child& actor : grandChildren)
		{
			for (const auto& component : this->components)
			{
				component->onChildActorDetached(actor.actor, actor.name);
			}
		}

		childPair.second->parentActor = nullptr;
	}

	this->childActorMap.clear();

	std::vector<std::unique_ptr<Actor>> detachedActors = std::move(this->childActors);
	this->childActors.clear();

	return detachedActors;
}

bool Actor::isParentTo(const Actor* childActor) const
{
	const auto childIt = std::find_if(
			this->childActors.begin(),
			this->childActors.end(),
			[childActor](const std::unique_ptr<Actor>& realChildActor)
			{
				return (realChildActor.get() == childActor);
			}
	);

	return (childIt != this->childActors.end());
}

bool Actor::isExtendedFrom(const std::string& definitionName) const
{
	if (this->definitionName == definitionName)
	{
		return true;
	}
	else
	{
		return (std::find(this->extendedFrom.begin(), extendedFrom.end(), definitionName) != this->extendedFrom.end());
	}
}

const std::vector<std::string>& Actor::getDefinitionsExtendedFrom() const
{
	return this->extendedFrom;
}

bool Actor::create()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == false && this->hasForcedActiveState() == false)
	{
		for (auto& childActor : this->childActors)
		{
			childActor->create();
		}

		for (auto& component : this->components)
		{
			component->onCreate();
		}

		std::unique_lock<std::mutex> eventQueueLock(this->queuedEventMutex);

		this->broadcastQueuedComponentEvents();

		eventQueueLock.unlock();

		this->created = true;

		// Make sure to create any child Actors attached during onCreate().
		for (auto& childActor : this->childActors)
		{
			childActor->create();
		}

		this->log.debug().raw("Created.");

		return true;
	}
	else
	{
		return false;
	}
}

void Actor::update(const Duration& deltaTime)
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	for (auto& component : this->components)
	{
		component->onUpdate(deltaTime);
	}

	for (auto& childActor : this->childActors)
	{
		childActor->update(deltaTime);
	}
}

Component* Actor::findComponent(const Component::IdType& name) const
{
	auto componentIt = this->componentMap.find(name);
	if (componentIt == this->componentMap.end())
	{
		return nullptr;
	}

	return componentIt->second;
}

void Actor::broadCastComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (this->isCreated() == true)
	{
		for (auto& component : this->components)
		{
			component->onComponentEvent(event);
		}
	}
	else
	{
		std::lock_guard<std::mutex> eventQueueLock(this->queuedEventMutex);

		if (this->isCreated() == true)
		{
			for (auto& component : this->components)
			{
				component->onComponentEvent(event);
			}
		}
		else
		{
			this->queuedComponentEvents.push(event);
		}
	}
}

void Actor::serialize(Json::Value& jsonObject, const bool extendDefinition) const
{
	if (extendDefinition == true && this->definitionName.empty() == false)
	{
		jsonObject["extend"] = this->definitionName;
	}
	else if (this->extendedFrom.empty() == false)
	{
		jsonObject["extend"] = this->extendedFrom.front();
	}

	if (this->name.empty() == false)
	{
		jsonObject["name"] = this->name;
	}

	if (this->trivial == true)
	{
		jsonObject["trivial"] = this->trivial;
	}

	for (const auto& component : this->components)
	{
		component->serialize(jsonObject["components"][component->getName()]);
	}

	if (this->childActorMap.empty() == false)
	{
		Json::Value& childActorsJson = jsonObject["childActors"];

		for (const auto& childActorPair : this->childActorMap)
		{
			childActorPair.second->serialize(childActorsJson[childActorPair.first], extendDefinition);
		}
	}
}

bool Actor::destroy()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->isActive() == false && this->hasForcedActiveState() == false)
	{
		for (auto& component : this->components)
		{
			component->onDestroy();
		}

		for (auto& childActor : this->childActors)
		{
			childActor->deactivate();
			childActor->destroy();
		}

		this->created = false;

		this->log.debug().raw("Destroyed.");

		return true;
	}
	else
	{
		return false;
	}
}

void Actor::activate()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->active == false)
	{
		this->active = true;

		if (this->forcedActiveState == ForcedActiveState::NONE)
		{
			for (auto& component : this->components)
			{
				component->onActivate();
			}

			this->log.debug().raw("Activated.");
		}
	}
}

void Actor::deactivate()
{
	std::lock_guard<std::mutex> stateLock(this->stateMutex);

	if (this->isCreated() == true && this->active == true)
	{
		this->active = false;

		if (this->forcedActiveState == ForcedActiveState::NONE)
		{
			for (auto& component : this->components)
			{
				component->onDeactivate();
			}

			this->log.debug().raw("Deactivated.");
		}
	}
}

void Actor::forceActiveState(const ForcedActiveState state)
{
	if (this->forcedActiveState != state)
	{
		if (state == ForcedActiveState::ACTIVE && this->active == false)
		{
			if (this->created == false)
			{
				for (auto& component : this->components)
				{
					component->onCreate();
				}

				std::lock_guard<std::mutex> eventQueueLock(this->queuedEventMutex);

				this->broadcastQueuedComponentEvents();
			}

			for (auto& component : this->components)
			{
				component->onActivate();
			}
		}
		else if (state == ForcedActiveState::INACTIVE && this->active == true)
		{
			for (auto& component : this->components)
			{
				component->onDeactivate();
			}
		}
		else if (this->forcedActiveState == ForcedActiveState::ACTIVE && this->active == false)
		{
			for (auto& component : this->components)
			{
				component->onDeactivate();
			}

			if (this->created == false)
			{
				for (auto& component : this->components)
				{
					component->onDestroy();
				}
			}
		}
		else if (this->forcedActiveState == ForcedActiveState::INACTIVE && this->active == true)
		{
			for (auto& component : this->components)
			{
				component->onActivate();
			}
		}

		this->forcedActiveState = state;

		for (auto& childActor : this->childActors)
		{
			childActor->forceActiveState(this->forcedActiveState);
		}
	}
}

void Actor::setName(const std::string& name)
{
	this->name = name;
}

void Actor::makeTrivial(const bool trivial)
{
	this->trivial = trivial;
}

Actor* Actor::findChildActor(const std::string& actorName) const
{
	const std::string::size_type childNameEndPos = actorName.find('.');

	if (childNameEndPos == std::string::npos)
	{
		auto actorIt = this->childActorMap.find(actorName);
		if (actorIt != this->childActorMap.end())
		{
			return actorIt->second;
		}
		else
		{
			return nullptr;
		}
	}
	else
	{
		const std::string childName = actorName.substr(0, childNameEndPos);

		auto actorIt = this->childActorMap.find(childName);
		if (actorIt != this->childActorMap.end())
		{
			const std::string::size_type grandChildNameStartPos = childNameEndPos + 1;
			const std::string grandChildName = actorName.substr(grandChildNameStartPos);

			return actorIt->second->findChildActor(grandChildName);
		}
		else
		{
			return nullptr;
		}
	}
}

std::vector<Actor::Child> Actor::findChildrenRecursively() const
{
	std::vector<Child> children;

	for (const auto& child : this->childActorMap)
	{
		children.push_back(Child(child.first, child.second));

		const std::vector<Child> grandChildren = child.second->findChildrenRecursively(child.first);
		children.insert(children.begin(), grandChildren.begin(), grandChildren.end());
	}

	return children;
}

std::vector<Actor::Child> Actor::findChildrenRecursively(const std::string& childName) const
{
	std::vector<Child> children;
	for (const auto& child : this->childActorMap)
	{
		const std::string grandChildName = childName + "." + child.first;

		children.push_back(Child(grandChildName, child.second));

		const std::vector<Child> grandChildren = child.second->findChildrenRecursively(grandChildName);
		children.insert(children.begin(), grandChildren.begin(), grandChildren.end());
	}

	return children;
}

void Actor::broadcastQueuedComponentEvents()
{
	while (this->queuedComponentEvents.empty() == false)
	{
		const std::shared_ptr<event::Event>& event = this->queuedComponentEvents.front();

		if (event != nullptr)
		{
			for (auto& component : this->components)
			{
				component->onComponentEvent(event);
			}
		}

		this->queuedComponentEvents.pop();
	}
}

} } }
