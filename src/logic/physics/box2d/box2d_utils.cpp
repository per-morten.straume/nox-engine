/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/box2d_utils.h>

namespace nox { namespace logic { namespace physics
{

float calculatePolygonArea(const b2Vec2* vs, int32 count)
{
	float32 area = 0.0f;

	b2Vec2 pRef(0.0f, 0.0f);

	for (int32 i = 0; i < count; ++i)
	{
		// Triangle vertices.
		b2Vec2 p1 = pRef;
		b2Vec2 p2 = vs[i];
		b2Vec2 p3 = i + 1 < count ? vs[i+1] : vs[0];

		b2Vec2 e1 = p2 - p1;
		b2Vec2 e2 = p3 - p1;

		float32 D = b2Cross(e1, e2);

		float32 triangleArea = 0.5f * D;
		area += triangleArea;
	}

	return area;
}

// Basically a copy of b2PolygonShape::Set(const b2Vec2* vertices, int32 count)
bool validPolygon(const b2Vec2* vertices, int32 count)
{
	if (count < 3 || count > b2_maxPolygonVertices)
	{
		return false;
	}

	if (vertices == nullptr)
	{
		return false;
	}

	int32 numVertices = b2Min(count, b2_maxPolygonVertices);
	b2Vec2 ps[b2_maxPolygonVertices];
	int32 uniqueVertices = 0;

	for (int32 i = 0; i < numVertices; i++)
	{
		const b2Vec2& vertex = vertices[i];

		bool unique = true;
		for (int32 j = 0; j < uniqueVertices; j++)
		{
			if (b2DistanceSquared(vertex, ps[j]) < 0.5f * b2_linearSlop)
			{
				unique = false;
				break;
			}
		}

		if (unique == true)
		{
			ps[uniqueVertices] = vertex;
			uniqueVertices++;
		}
	}

	if (uniqueVertices < 3)
	{
		return false;
	}

	int32 i0 = 0;
	float32 x0 = ps[0].x;
	for (int32 i = 1; i < uniqueVertices; ++i)
	{
		float32 x = ps[i].x;
		if (x > x0 || (x == x0 && ps[i].y < ps[i0].y))
		{
			i0 = i;
			x0 = x;
		}
	}

	int32 hull[b2_maxPolygonVertices];
	int32 m = 0;
	int32 ih = i0;

	for (;;)
	{
		hull[m] = ih;

		int32 ie = 0;
		for (int32 j = 1; j < uniqueVertices; ++j)
		{
			if (ie == ih)
			{
				ie = j;
				continue;
			}

			b2Vec2 r = ps[ie] - ps[hull[m]];
			b2Vec2 v = ps[j] - ps[hull[m]];
			float32 c = b2Cross(r, v);
			if (c < 0.0f)
			{
				ie = j;
			}

			// Collinearity check
			if (c == 0.0f && v.LengthSquared() > r.LengthSquared())
			{
				ie = j;
			}
		}

		++m;
		ih = ie;

		if (ie == i0)
		{
			break;
		}
	}

	if (m < 3)
	{
		return false;
	}

	if (calculatePolygonArea(vertices, count) < b2_epsilon)
	{
		return false;
	}

	return true;
}

bool pointIsInsideBox(const b2Vec2& point, const b2AABB& box)
{
	if (point.x < box.lowerBound.x)
	{
		return false;
	}

	if (point.y < box.lowerBound.y)
	{
		return false;
	}

	if (point.x > box.upperBound.x)
	{
		return false;
	}

	if (point.y > box.upperBound.y)
	{
		return false;
	}

	return true;
}

}
}
}
