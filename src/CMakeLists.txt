if (NOX_WARNINGS)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${NOX_WARNINGS}")
endif ()

set(NOX_COMMON_SOURCES
	${NOX_INCLUDE_DIR}/nox/common/api.h
	${NOX_INCLUDE_DIR}/nox/common/types.h
	${NOX_INCLUDE_DIR}/nox/common/platform.h
	${NOX_INCLUDE_DIR}/nox/common/compat.h
)

set(NOX_UTIL_SOURCES
	${NOX_INCLUDE_DIR}/nox/util/process/Manager.h
	${NOX_INCLUDE_DIR}/nox/util/process/Implementation.h
	${NOX_INCLUDE_DIR}/nox/util/math/exponential.h
	${NOX_INCLUDE_DIR}/nox/util/math/equality.h
	${NOX_INCLUDE_DIR}/nox/util/math/physics.h
	${NOX_INCLUDE_DIR}/nox/util/math/interpolate.h
	${NOX_INCLUDE_DIR}/nox/util/math/angle.h
	${NOX_INCLUDE_DIR}/nox/util/math/vector_math.h
	${NOX_INCLUDE_DIR}/nox/util/math/Circle.h
	${NOX_INCLUDE_DIR}/nox/util/math/Box.h
	${NOX_INCLUDE_DIR}/nox/util/math/Line.h
	${NOX_INCLUDE_DIR}/nox/util/math/geometry.h
	${NOX_INCLUDE_DIR}/nox/util/thread/ThreadBarrier.h
	${NOX_INCLUDE_DIR}/nox/util/thread/ThreadSafeMap.h
	${NOX_INCLUDE_DIR}/nox/util/thread/ThreadSafeQueue.h
	${NOX_INCLUDE_DIR}/nox/util/chrono_utils.h
	${NOX_INCLUDE_DIR}/nox/util/string_utils.h
	${NOX_INCLUDE_DIR}/nox/util/Buffer.h
	${NOX_INCLUDE_DIR}/nox/util/Timer.h
	${NOX_INCLUDE_DIR}/nox/util/boost_utils.h
	${NOX_INCLUDE_DIR}/nox/util/Booker.h
	${NOX_INCLUDE_DIR}/nox/util/KeyBooker.h
	util/process/Process.h
	util/process/Process.cpp
	util/process/Manager.cpp
	util/process/Implementation.cpp
	util/math/angle.cpp
	util/math/vector_math.cpp
	util/math/geometry.cpp
	util/thread/ThreadBarrier.cpp
	util/boost_utils.cpp
	util/string_utils.cpp
	util/match_wildcard.cpp
)

set(NOX_APP_SOURCES
	${NOX_INCLUDE_DIR}/nox/app/IContext.h
	${NOX_INCLUDE_DIR}/nox/app/Application.h
	${NOX_INCLUDE_DIR}/nox/app/ApplicationProcess.h
	${NOX_INCLUDE_DIR}/nox/app/platform.h
	app/IContext.cpp
	app/Application.cpp
	app/ApplicationProcess.cpp
	app/platform.cpp
)

set(NOX_LOG_SOURCES
	${NOX_INCLUDE_DIR}/nox/log/Logger.h
	${NOX_INCLUDE_DIR}/nox/log/Message.h
	${NOX_INCLUDE_DIR}/nox/log/Output.h
	${NOX_INCLUDE_DIR}/nox/log/OutputStream.h
	${NOX_INCLUDE_DIR}/nox/log/OutputManager.h
	log/Logger.cpp
	log/Message.cpp
	log/Output.cpp
	log/OutputStream.cpp
	log/OutputManager.cpp
)

set(NOX_APP_RESOURCE_SOURCES
	${NOX_INCLUDE_DIR}/nox/app/resource/util.h
	${NOX_INCLUDE_DIR}/nox/app/resource/Handle.h
	${NOX_INCLUDE_DIR}/nox/app/resource/File.h
	${NOX_INCLUDE_DIR}/nox/app/resource/Descriptor.h
	${NOX_INCLUDE_DIR}/nox/app/resource/IHandleDestructionListener.h
	${NOX_INCLUDE_DIR}/nox/app/resource/IResourceAccess.h
	${NOX_INCLUDE_DIR}/nox/app/resource/data/IExtraData.h
	${NOX_INCLUDE_DIR}/nox/app/resource/data/SoundExtraData.h
	${NOX_INCLUDE_DIR}/nox/app/resource/loader/ILoader.h
	${NOX_INCLUDE_DIR}/nox/app/resource/provider/Provider.h
	${NOX_INCLUDE_DIR}/nox/app/resource/cache/Cache.h
	${NOX_INCLUDE_DIR}/nox/app/resource/cache/LruCache.h
	app/resource/util.cpp
	app/resource/Handle.cpp
	app/resource/File.cpp
	app/resource/Descriptor.cpp
	app/resource/IHandleDestructionListener.cpp
	app/resource/IResourceAccess.cpp
	app/resource/data/IExtraData.cpp
	app/resource/data/SoundExtraData.cpp
	app/resource/loader/ILoader.cpp
	app/resource/loader/DefaultLoader.h
	app/resource/loader/DefaultLoader.cpp
	app/resource/provider/Provider.cpp
	app/resource/cache/Cache.cpp
	app/resource/cache/LruCache.cpp
)

set(NOX_APP_AUDIO_SOURCES
	${NOX_INCLUDE_DIR}/nox/app/audio/System.h
	${NOX_INCLUDE_DIR}/nox/app/audio/Buffer.h
	${NOX_INCLUDE_DIR}/nox/app/audio/PlaybackProcess.h
	app/audio/System.cpp
	app/audio/Buffer.cpp
	app/audio/PlaybackProcess.cpp
)

set(NOX_APP_STORAGE_SOURCES
	${NOX_INCLUDE_DIR}/nox/app/storage/IDataStorage.h
	${NOX_INCLUDE_DIR}/nox/app/storage/MemoryDataStorage.h
	app/storage/IDataStorage.cpp
	app/storage/MemoryDataStorage.cpp
)

set(NOX_APP_GRAPHICS_SOURCES
	${NOX_INCLUDE_DIR}/nox/app/graphics/render_utils.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/SubRenderer.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/TextureAtlasReader.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/TexturePackerTextureAtlasReader.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/IRenderer.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/Geometry.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/GeometrySet.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/Light.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/Camera.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/SceneGraphNode.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/SpriteRenderNode.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/WrappedSpriteRenderNode.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/LightRenderNode.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/TransformationNode.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/TiledTextureGenerator.h
	${NOX_INCLUDE_DIR}/nox/app/graphics/2d/StenciledTiledTextureGenerator.h
	app/graphics/render_utils.cpp
	app/graphics/SubRenderer.cpp
	app/graphics/TextureAtlasReader.cpp
	app/graphics/TexturePackerTextureAtlasReader.cpp
	app/graphics/2d/IRenderer.cpp
	app/graphics/2d/Geometry.cpp
	app/graphics/2d/GeometrySet.cpp
	app/graphics/2d/Light.cpp
	app/graphics/2d/Camera.cpp
	app/graphics/2d/SceneGraphNode.cpp
	app/graphics/2d/SpriteRenderNode.cpp
	app/graphics/2d/WrappedSpriteRenderNode.cpp
	app/graphics/2d/LightRenderNode.cpp
	app/graphics/2d/TransformationNode.cpp
	app/graphics/2d/TiledTextureGenerator.cpp
	app/graphics/2d/StenciledTiledTextureGenerator.cpp
)

set(NOX_LOGIC_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/IContext.h
	${NOX_INCLUDE_DIR}/nox/logic/Logic.h
	${NOX_INCLUDE_DIR}/nox/logic/View.h
	logic/IContext.cpp
	logic/Logic.cpp
	logic/View.cpp
)

set(NOX_EVENT_SOURCES
	${NOX_INCLUDE_DIR}/nox/event/Event.h
	${NOX_INCLUDE_DIR}/nox/event/BooleanEvent.h
	${NOX_INCLUDE_DIR}/nox/event/IBroadcaster.h
	${NOX_INCLUDE_DIR}/nox/event/Manager.h
	${NOX_INCLUDE_DIR}/nox/event/IListener.h
	${NOX_INCLUDE_DIR}/nox/event/ListenerManager.h
	event/Event.cpp
	event/BooleanEvent.cpp
	event/IBroadcaster.cpp
	event/Manager.cpp
	event/IListener.cpp
	event/ListenerManager.cpp
)

set(NOX_LOGIC_ACTOR_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/actor/Identifier.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/Actor.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/Component.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/Factory.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/component/Transform.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/event/Event.h
	${NOX_INCLUDE_DIR}/nox/logic/actor/event/TransformChange.h
	logic/actor/Identifier.cpp
	logic/actor/Actor.cpp
	logic/actor/Component.cpp
	logic/actor/Factory.cpp
	logic/actor/component/Transform.cpp
	logic/actor/event/Event.cpp
	logic/actor/event/TransformChange.cpp
)

set(NOX_LOGIC_PHYSICS_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/physics/Simulation.h
	${NOX_INCLUDE_DIR}/nox/logic/physics/physics_utils.h
	${NOX_INCLUDE_DIR}/nox/logic/physics/joint.h
	${NOX_INCLUDE_DIR}/nox/logic/physics/GravitationalForce.h
	${NOX_INCLUDE_DIR}/nox/logic/physics/actor/ActorPhysics.h
	${NOX_INCLUDE_DIR}/nox/logic/physics/actor/ActorGravitation.h
	logic/physics/Simulation.cpp
	logic/physics/physics_utils.cpp
	logic/physics/GravitationalForce.cpp
	logic/physics/actor/ActorPhysics.cpp
	logic/physics/actor/ActorGravitation.cpp
)

set(NOX_LOGIC_WORLD_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/world/Manager.h
	${NOX_INCLUDE_DIR}/nox/logic/world/Writer.h
	${NOX_INCLUDE_DIR}/nox/logic/world/Loader.h
	${NOX_INCLUDE_DIR}/nox/logic/world/event/ActorCreated.h
	${NOX_INCLUDE_DIR}/nox/logic/world/event/ActorRemoved.h
	logic/world/Manager.cpp
	logic/world/Writer.cpp
	logic/world/Loader.cpp
	logic/world/event/ActorCreated.cpp
	logic/world/event/ActorRemoved.cpp
)

set(NOX_LOGIC_GRAPHICS_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/graphics/actor/ActorGraphics.h
	${NOX_INCLUDE_DIR}/nox/logic/graphics/actor/ActorSprite.h
	${NOX_INCLUDE_DIR}/nox/logic/graphics/actor/ActorLight.h
	${NOX_INCLUDE_DIR}/nox/logic/graphics/event/DebugRenderingEnabled.h
	${NOX_INCLUDE_DIR}/nox/logic/graphics/event/DebugGeometryChange.h
	${NOX_INCLUDE_DIR}/nox/logic/graphics/event/SceneNodeEdited.h
	logic/graphics/actor/ActorGraphics.cpp
	logic/graphics/actor/ActorSprite.cpp
	logic/graphics/actor/ActorLight.cpp
	logic/graphics/event/DebugRenderingEnabled.cpp
	logic/graphics/event/DebugGeometryChange.cpp
	logic/graphics/event/SceneNodeEdited.cpp
)

set(NOX_LOGIC_CONTROL_SOURCES
	${NOX_INCLUDE_DIR}/nox/logic/control/actor/ActorControl.h
	${NOX_INCLUDE_DIR}/nox/logic/control/actor/Actor2dDirectionControl.h
	${NOX_INCLUDE_DIR}/nox/logic/control/actor/Actor2dRotationControl.h
	${NOX_INCLUDE_DIR}/nox/logic/control/event/Action.h
	logic/control/actor/ActorControl.cpp
	logic/control/actor/Actor2dDirectionControl.cpp
	logic/control/actor/Actor2dRotationControl.cpp
	logic/control/event/Action.cpp
)

set(NOX_WINDOW_SOURCES
)

if (NOX_RESOURCEPROVIDER_BOOST)
	message(STATUS "NOX Engine: Building Boost.Filesystem based resource provider.")

	set(NOX_DEPEND_BOOST_FILESYSTEM ON)

	list(APPEND NOX_APP_RESOURCE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/resource/provider/BoostFilesystemProvider.h
		app/resource/provider/BoostFilesystemProvider.cpp
	)
endif ()

if (NOX_RESOURCE_JSON)
	message(STATUS "NOX Engine: Building JSON resource loader.")

	set(NOX_DEPEND_JSONCPP ON)

	list(APPEND NOX_APP_RESOURCE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/resource/loader/JsonLoader.h
		${NOX_INCLUDE_DIR}/nox/app/resource/data/JsonExtraData.h
		${NOX_INCLUDE_DIR}/nox/util/json_utils.h
		app/resource/loader/JsonLoader.cpp
		app/resource/data/JsonExtraData.cpp
	)
endif ()

if (NOX_RESOURCE_OGG)
	message(STATUS "NOX Engine: Building ogg resource loader.")

	set(NOX_DEPEND_OGG ON)
	set(NOX_DEPEND_VORBIS_FILE ON)

	list(APPEND NOX_APP_RESOURCE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/resource/loader/OggLoader.h
		app/resource/loader/OggLoader.cpp
	)
endif ()

if (NOX_AUDIO_OPENAL)
	message(STATUS "NOX Engine: Building OpenAL audio backend.")

	set(NOX_DEPEND_OPENAL ON)

	list(APPEND NOX_APP_AUDIO_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/audio/openal/OpenALSystem.h
		${NOX_INCLUDE_DIR}/nox/app/audio/openal/OpenALBuffer.h
		app/audio/openal/OpenALSystem.cpp
		app/audio/openal/OpenALBuffer.cpp
	)
endif ()

if (NOX_GRAPHICS_OPENGL OR NOX_GRAPHICS_GLES2 OR NOX_GRAPHICS_GLES3)
	message(STATUS "NOX Engine: Building OpenGL graphics backend.")

	if (${NOX_OPENGL_TYPE} STREQUAL "ES")
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_ES=1")
	else ()
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_DESKTOP=1")
	endif ()

	if (NOX_OPENGL_GLEW)
		set(NOX_DEPEND_GLEW ON)
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_GLEW=1")
	endif ()

	list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_VERSION_MAJOR=${NOX_OPENGL_VERSION_MAJOR}")
	list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_VERSION_MINOR=${NOX_OPENGL_VERSION_MINOR}")

	list(APPEND NOX_APP_GRAPHICS_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/graphics/opengl_utils.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/RenderData.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/TextureQuad.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/TextureManager.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/TextureRenderer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlDataType.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlContextState.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlBuffer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlBufferUploader.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlBlockingBufferUploader.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlVertexAttribute.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlVertexModelStorage.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlVertexModel.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/GlIndexedVertexModel.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/2d/OpenGlRenderer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/2d/TiledTextureRenderer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/2d/StenciledTiledTextureRenderer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/2d/DebugRenderer.h
		${NOX_INCLUDE_DIR}/nox/app/graphics/2d/BackgroundGradient.h
		app/graphics/opengl_utils.cpp
		app/graphics/RenderData.cpp
		app/graphics/TextureQuad.cpp
		app/graphics/TextureManager.cpp
		app/graphics/TextureRenderer.cpp
		app/graphics/GlDataType.cpp
		app/graphics/GlContextState.cpp
		app/graphics/GlBuffer.cpp
		app/graphics/GlBufferUploader.cpp
		app/graphics/GlBlockingBufferUploader.cpp
		app/graphics/GlVertexAttribute.cpp
		app/graphics/GlVertexModelStorage.cpp
		app/graphics/GlVertexModel.cpp
		app/graphics/GlIndexedVertexModel.cpp
		app/graphics/2d/OpenGlRenderer.cpp
		app/graphics/2d/TiledTextureRenderer.cpp
		app/graphics/2d/StenciledTiledTextureRenderer.cpp
		app/graphics/2d/DebugRenderer.cpp
		app/graphics/2d/BackgroundGradient.cpp
	)

	if (NOX_OPENGL_SUPPORT_MULTISAMPLE)
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_SUPPORT_MULTISAMPLE=1")
	endif ()
	if (NOX_OPENGL_SUPPORT_VAO)
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_SUPPORT_VAO=1")
		list(APPEND NOX_APP_GRAPHICS_SOURCES
			${NOX_INCLUDE_DIR}/nox/app/graphics/GlVertexArray.h
			${NOX_INCLUDE_DIR}/nox/app/graphics/GlVertexModelStorageVao.h
			app/graphics/GlVertexArray.cpp
			app/graphics/GlVertexModelStorageVao.cpp
		)
	endif ()
	if (NOX_OPENGL_SUPPORT_MAPBUFFER)
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_SUPPORT_MAPBUFFER=1")
		list(APPEND NOX_APP_GRAPHICS_SOURCES
			${NOX_INCLUDE_DIR}/nox/app/graphics/GlAsyncBufferUploader.h
			app/graphics/GlAsyncBufferUploader.cpp
		)
	endif ()
	if (NOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS)
		list(APPEND NOX_COMPILE_DEFINITIONS "-DNOX_OPENGL_SUPPORT_MULTICOLORATTACHMENTS=1")
	endif ()
endif ()

if (NOX_WINDOW_SDL2)
	message(STATUS "NOX Engine: Building SDL2 window backend.")

	set(NOX_APP_SDL2 ON CACHE BOOL "" FORCE)
	set(NOX_DEPEND_SDL2 ON)

	list(APPEND NOX_WINDOW_SOURCES
		${NOX_INCLUDE_DIR}/nox/window/SdlWindowView.h
		${NOX_INCLUDE_DIR}/nox/window/RenderSdlWindowView.h
		${NOX_INCLUDE_DIR}/nox/window/SdlKeyboardControlMapper.h
		window/SdlWindowView.cpp
		window/RenderSdlWindowView.cpp
		window/SdlKeyboardControlMapper.cpp
	)
endif ()

if (NOX_APP_SDL2)
	message(STATUS "NOX Engine: Building SDL2 applciation backend.")

	set(NOX_DEPEND_SDL2 ON)

	list(APPEND NOX_APP_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/SdlApplication.h
		app/SdlApplication.cpp
	)
endif ()

if (NOX_STORAGE_BOOST)
	message(STATUS "NOX Engine: Building Boost.Filesystem storage backend.")

	set(NOX_DEPEND_BOOST_FILESYSTEM ON)

	list(APPEND NOX_APP_STORAGE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/storage/DataStorageBoost.h
		app/storage/DataStorageBoost.cpp
	)
endif ()

if (NOX_PHYSICS_BOX2D)
	message(STATUS "NOX Engine: Building Box2D physics backend.")

	set(NOX_DEPEND_BOX2D ON)

	list(APPEND NOX_LOGIC_PHYSICS_SOURCES
		${NOX_INCLUDE_DIR}/nox/logic/physics/box2d/Box2DSimulation.h
		${NOX_INCLUDE_DIR}/nox/logic/physics/box2d/box2d_utils.h
		logic/physics/box2d/Box2DSimulation.cpp
		logic/physics/box2d/box2d_utils.cpp
	)
endif ()

if (ANDROID)
	message(STATUS "NOX Engine: Building Android backends.")

	set(NOX_DEPEND_ANDROID ON)

	list(APPEND NOX_UTIL_SOURCES
		${NOX_INCLUDE_DIR}/nox/util/android/AssetManager.h
		${NOX_INCLUDE_DIR}/nox/util/android/AssetFile.h
		${NOX_INCLUDE_DIR}/nox/util/android/FileInputBuffer.h
		${NOX_INCLUDE_DIR}/nox/util/android/FileOutputBuffer.h
		${NOX_INCLUDE_DIR}/nox/util/android/FileInputStream.h
		${NOX_INCLUDE_DIR}/nox/util/android/FileOutputStream.h
		util/android/AssetManager.cpp
		util/android/AssetFile.cpp
		util/android/FileInputBuffer.cpp
		util/android/FileOutputBuffer.cpp
		util/android/FileInputStream.cpp
		util/android/FileOutputStream.cpp
	)

	list(APPEND NOX_LOG_SOURCES
		${NOX_INCLUDE_DIR}/nox/log/LogcatOutput.h
		log/LogcatOutput.cpp
	)

	list(APPEND NOX_APP_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/SdlAndroidActivity.h
		app/SdlAndroidActivity.cpp
	)

	list(APPEND NOX_APP_RESOURCE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/resource/provider/AndroidAssetProvider.h
		app/resource/provider/AndroidAssetProvider.cpp
	)

	list(APPEND NOX_APP_STORAGE_SOURCES
		${NOX_INCLUDE_DIR}/nox/app/storage/AndroidDataStorage.h
		app/storage/AndroidDataStorage.cpp
	)
endif ()

set(NOX_SOURCES
	${NOX_COMMON_SOURCES}
	${NOX_UTIL_SOURCES}
	${NOX_APP_SOURCES}
	${NOX_LOG_SOURCES}
	${NOX_APP_RESOURCE_SOURCES}
	${NOX_APP_AUDIO_SOURCES}
	${NOX_APP_GRAPHICS_SOURCES}
	${NOX_APP_STORAGE_SOURCES}
	${NOX_LOGIC_SOURCES}
	${NOX_EVENT_SOURCES}
	${NOX_LOGIC_ACTOR_SOURCES}
	${NOX_LOGIC_PHYSICS_SOURCES}
	${NOX_LOGIC_WORLD_SOURCES}
	${NOX_LOGIC_GRAPHICS_SOURCES}
	${NOX_LOGIC_CONTROL_SOURCES}
	${NOX_WINDOW_SOURCES}
)

source_group(common FILES ${NOX_COMMON_SOURCES})
source_group(util FILES ${NOX_UTIL_SOURCES})
source_group(log FILES ${NOX_LOG_SOURCES})
source_group(event FILES ${NOX_EVENT_SOURCES})
source_group(app FILES ${NOX_APP_SOURCES})
source_group(app\\resource FILES ${NOX_APP_RESOURCE_SOURCES})
source_group(app\\audio FILES ${NOX_APP_AUDIO_SOURCES})
source_group(app\\graphics FILES ${NOX_APP_GRAPHICS_SOURCES})
source_group(app\\storage FILES ${NOX_APP_STORAGE_SOURCES})
source_group(logic FILES ${NOX_LOGIC_SOURCES})
source_group(logic\\actor FILES ${NOX_LOGIC_ACTOR_SOURCES})
source_group(logic\\physics FILES ${NOX_LOGIC_PHYSICS_SOURCES})
source_group(logic\\world FILES ${NOX_LOGIC_WORLD_SOURCES})
source_group(logic\\graphics FILES ${NOX_LOGIC_GRAPHICS_SOURCES})
source_group(logic\\control FILES ${NOX_LOGIC_CONTROL_SOURCES})
source_group(window FILES ${NOX_WINDOW_SOURCES})

# Boost.Geometry is used several places in the engine (especially physics and graphics), so this is a requirement.
set(NOX_DEPEND_BOOST ON)

# poly2tri is used several places in the engine (especially physics), so this is a requirement.
set(NOX_DEPEND_POLY2TRI ON)

# glm is a project requirement.
set(NOX_DEPEND_GLM ON)

# cppformat is a requirement of the log subsystem.
set(NOX_DEPEND_CPPFORMAT ON)

if (NOX_DEPEND_BOOST_FILESYSTEM)
	message(STATUS "NOX Engine: Using Boost.Filesystem library with Boost.System and Boost.Locale.")

	# Also depend on the locale library to properly support UTF-8 encoding.
	list(APPEND NOX_BOOST_COMPONENTS system filesystem locale)
endif()

if (NOX_DEPEND_BOOST)
	message(STATUS "NOX Engine: Using Boost library.")

	set(Boost_USE_STATIC_LIBS OFF)
	set(Boost_USE_STATIC_RUNTIME OFF)

	if (NOX_BOOST_COMPONENTS)
		if (ANDROID)
			message(FATAL_ERROR "NOX Engine does not yet support building Boost for Android. Only headers can be used.")
		endif ()

		find_package(Boost REQUIRED COMPONENTS ${NOX_BOOST_COMPONENTS})
	else ()
		if (ANDROID)
			if (NOT BOOST_INCLUDEDIR)
				message(FATAL_ERROR "Include dir for Boost not specified. Specify it with BOOST_INCLUDEDIR")
			else ()
				list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${BOOST_INCLUDEDIR})
			endif ()
		else ()
			find_package(Boost REQUIRED)
		endif ()
	endif ()

	# Force dynamic linking rather than the default static linking, and disable autolink.
	list(APPEND NOX_COMPILE_DEFINITIONS -DBOOST_ALL_DYN_LINK -DBOOST_ALL_NO_LIB)
	list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
	list(APPEND NOX_EXTERNAL_LIBRARIES ${Boost_LIBRARIES})
endif ()

if (NOX_DEPEND_JSONCPP)
	message(STATUS "NOX Engine: Using jsoncpp library.")

	list(APPEND NOX_EXTERNAL_LIBRARIES ${NOX_JSONCPP_TARGET})
	list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:${NOX_JSONCPP_TARGET},INTERFACE_INCLUDE_DIRECTORIES>)
endif()

if (NOX_DEPEND_OGG)
	message(STATUS "NOX Engine: Using ogg library.")

	if (NOT NOX_USE_SYSTEM_LIBS)
		list(APPEND NOX_EXTERNAL_LIBRARIES libogg)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:libogg,INTERFACE_INCLUDE_DIRECTORIES>)
	else ()
		find_package(OGG REQUIRED)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${OGG_LIBRARIES})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${OGG_INCLUDE_DIR})
	endif ()
endif()

if (NOX_DEPEND_VORBIS_FILE)
	message(STATUS "NOX Engine: Using vorbisfile library.")

	if (NOT NOX_USE_SYSTEM_LIBS)
		list(APPEND NOX_EXTERNAL_LIBRARIES libvorbis)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:libvorbis,INTERFACE_INCLUDE_DIRECTORIES>)
	else ()
		find_package(VorbisFile REQUIRED)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${VORBISFILE_LIBRARIES})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${VORBISFILE_INCLUDE_DIR})
	endif ()
endif()

if (NOX_DEPEND_OPENAL)
	message(STATUS "NOX Engine: Using OpenAL library.")

	if (NOT NOX_USE_SYSTEM_LIBS)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${NOX_OPENAL_TARGET})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:${NOX_OPENAL_TARGET},INTERFACE_INCLUDE_DIRECTORIES>)
	else ()
		find_package(OpenAL REQUIRED)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${OPENAL_LIBRARY})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${OPENAL_INCLUDE_DIR})
	endif ()
endif()

if (NOX_DEPEND_SDL2)
	message(STATUS "NOX Engine: Using SDL2 library.")

	if (NOT NOX_USE_SYSTEM_LIBS)
		if (NOX_BUILD_SHARED)
			list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES SDL2)
			list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:SDL2,INTERFACE_INCLUDE_DIRECTORIES>)
		endif ()
		if (NOX_BUILD_STATIC)
			list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES SDL2-static)
			list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:SDL2-static,INTERFACE_INCLUDE_DIRECTORIES>)
		endif ()

		if (MSVC)
			list(APPEND NOX_EXTERNAL_LIBRARIES SDL2main)
		endif ()
	else ()
		find_package(SDL2 REQUIRED)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${SDL2_LIBRARY})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${SDL2_INCLUDE_DIR})
	endif ()
endif()

if (NOX_DEPEND_SDL2_IMAGE)
	message(STATUS "NOX Engine: Using SDL2_image library.")

	if (NOT NOX_USE_SYSTEM_LIBS)
		if (NOX_BUILD_SHARED)
			list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES SDL2_image-shared)
			list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:SDL2_image-shared,INTERFACE_INCLUDE_DIRECTORIES>)
		endif ()
		if (NOX_BUILD_STATIC)
			list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES SDL2_image-static)
			list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:SDL2_image-static,INTERFACE_INCLUDE_DIRECTORIES>)
		endif ()
	else ()
		find_package(SDL2_image REQUIRED)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${SDL2_IMAGE_LIBRARIES})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${SDL2_IMAGE_INCLUDE_DIRS})
	endif ()
endif()

if (NOX_DEPEND_LODEPNG)
	message(STATUS "NOX Engine: Using lodepng library.")

	if (NOX_BUILD_SHARED)
		list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES lodepng-shared)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:lodepng-shared,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
	if (NOX_BUILD_STATIC)
		list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES lodepng-static)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:lodepng-static,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
endif()

if (NOX_DEPEND_BOX2D)
	message(STATUS "NOX Engine: Using Box2D library.")

	if (NOX_BUILD_SHARED)
		list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES Box2D_shared)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:Box2D_shared,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
	if (NOX_BUILD_STATIC)
		list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES Box2D)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:Box2D,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
endif ()

if (NOX_DEPEND_POLY2TRI)
	message(STATUS "NOX Engine: Using poly2tri library.")

	if (NOX_BUILD_SHARED)
		list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES poly2tri-shared)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:poly2tri-shared,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
	if (NOX_BUILD_STATIC)
		list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES poly2tri-static)
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:poly2tri-static,INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
endif ()

if (NOX_DEPEND_GLM)
	message(STATUS "NOX Engine: Using glm library.")

	list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${NOX_GLM_INCLUDE_DIR})
endif ()

if (NOX_DEPEND_CPPFORMAT)
	message(STATUS "NOX Engine: Using fmt library.")

	list(APPEND NOX_EXTERNAL_LIBRARIES fmt)
	list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:fmt,INTERFACE_INCLUDE_DIRECTORIES>)
endif ()

if (NOX_DEPEND_ANDROID)
	message(STATUS "NOX Engine: Using android library.")

	list(APPEND NOX_EXTERNAL_LIBRARIES android)
endif ()

if (NOX_DEPEND_GLEW)
	message(STATUS "NOX Engine: Using GLEW library.")

	if (NOX_USE_SYSTEM_LIBS)
		find_package(GLEW)
		list(APPEND NOX_EXTERNAL_LIBRARIES ${GLEW_LIBRARIES})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${GLEW_INCLUDE_DIRS})
	else ()
		list(APPEND NOX_EXTERNAL_LIBRARIES ${NOX_GLEW_TARGET})
		list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:${NOX_GLEW_TARGET},INTERFACE_INCLUDE_DIRECTORIES>)
	endif ()
endif ()

list(APPEND NOX_SHARED_EXTERNAL_LIBRARIES
	${NOX_EXTERNAL_LIBRARIES}
)

list(APPEND NOX_STATIC_EXTERNAL_LIBRARIES
	${NOX_EXTERNAL_LIBRARIES}
)

list(APPEND NOX_COMPILE_DEFINITIONS
	$<$<OR:$<CONFIG:Debug>,$<CONFIG:RelWithDebInfo>>:NOX_DEBUG>
)

if (NOX_BUILD_SHARED)
	message(STATUS "NOX Engine: Building shared library.")
	add_library(nox-shared SHARED ${NOX_SOURCES})
	set_target_properties(nox-shared PROPERTIES
		VERSION ${NOX_VERSION}
		SOVERSION ${NOX_API_VERSION}
		OUTPUT_NAME ${NOX_LIB_NAME}
		FOLDER "NOX/lib"
	)

	target_link_libraries(nox-shared ${NOX_SHARED_EXTERNAL_LIBRARIES})
	target_include_directories(nox-shared PUBLIC ${NOX_INCLUDE_DIR})
	target_include_directories(nox-shared SYSTEM PUBLIC ${NOX_EXTERNAL_INCLUDE_DIRS})
	target_compile_definitions(nox-shared
		PRIVATE -DNOX_BUILDING_SHARED
		INTERFACE -DNOX_USING_SHARED
		PUBLIC ${NOX_COMPILE_DEFINITIONS}
	)

	if (NOX_COTIRE)
		cotire(nox-shared)
	endif ()
endif ()

if (NOX_BUILD_STATIC)
	message(STATUS "NOX Engine: Building static library.")
	add_library(nox-static STATIC ${NOX_SOURCES})
	set_target_properties(nox-static PROPERTIES
		VERSION ${NOX_VERSION}
		SOVERSION ${NOX_API_VERSION}
		OUTPUT_NAME ${NOX_LIB_NAME}
		FOLDER "NOX/lib"
	)

	target_link_libraries(nox-static ${NOX_STATIC_EXTERNAL_LIBRARIES})
	target_include_directories(nox-static PUBLIC ${NOX_INCLUDE_DIR})
	target_include_directories(nox-static SYSTEM PUBLIC ${NOX_EXTERNAL_INCLUDE_DIRS})
	target_compile_definitions(nox-static PUBLIC ${NOX_COMPILE_DEFINITIONS})

	if (NOX_COTIRE)
		cotire(nox-static)
	endif ()
endif ()

if (NOX_INSTALL)
	message(STATUS "NOX Engine: Configuring installation.")

	install(
		DIRECTORY ${NOX_INCLUDE_DIR}/
		DESTINATION include
	)

	if (NOX_BUILD_SHARED)
		install(
			TARGETS nox-shared
			LIBRARY DESTINATION lib
			ARCHIVE DESTINATION lib
			RUNTIME DESTINATION bin
		)
	endif ()

	if (NOX_BUILD_STATIC)
		install(
			TARGETS nox-static
			LIBRARY DESTINATION lib
			ARCHIVE DESTINATION lib
			RUNTIME DESTINATION bin
		)
	endif ()
endif ()
