add_executable(nox-test-application
	TestApplication.cpp
)

target_link_libraries(nox-test-application
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(nox-test-application PROPERTIES
    FOLDER ${NOX_TEST_FOLDER}
)

add_test(
	NAME nox-test-application
	COMMAND nox-test-application
)

add_subdirectory(log)
