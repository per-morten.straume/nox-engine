add_executable(nox-test-log
	TestLog.cpp
)

target_link_libraries(nox-test-log
	${NOX_GTEST_LIBRARY}
	${NOX_LINK_TARGET}
)

set_target_properties(nox-test-log PROPERTIES
    FOLDER ${NOX_TEST_FOLDER}
)

add_test(
	NAME nox-test-log
	COMMAND nox-test-log
)
