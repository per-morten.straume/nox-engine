/*
 * Copyright 2014 Suttung Digital AS.
 */

#include <gtest/gtest.h>
#include <nox/util/Mask.h>

enum class MaskType
{
	NONE = 0,
	MASK_1 = 1 << 0,
	MASK_2 = 1 << 1,
	MASK_3 = 1 << 2,
	MASK_4 = 1 << 3,
	MASK_5 = 1 << 4,
	MASK_6 = 1 << 5,
	MASK_ALL = ~0x0
};

using Underlaying = std::underlying_type<MaskType>::type;

TEST(TestMask, TestBasicConstruction)
{
	nox::util::Mask<MaskType> mask;

	mask = nox::util::Mask<MaskType>(MaskType::MASK_2);
	EXPECT_EQ(MaskType::MASK_2, mask.getRawMask());

	mask = MaskType::MASK_2;
	EXPECT_EQ(MaskType::MASK_2, mask.getRawMask());

	mask = MaskType::MASK_ALL;
	EXPECT_EQ(MaskType::MASK_ALL, mask.getRawMask());

	mask = nox::util::Mask<MaskType>();
	EXPECT_EQ(MaskType::NONE, mask.getRawMask());
}

TEST(TestMask, TestListConstruction)
{
	nox::util::Mask<MaskType> mask;

	mask = nox::util::Mask<MaskType>({ MaskType::MASK_1, MaskType::MASK_5 });
	EXPECT_EQ(Underlaying(MaskType::MASK_1) | Underlaying(MaskType::MASK_5), Underlaying(mask.getRawMask()));

	mask = { MaskType::MASK_1, MaskType::MASK_5 };
	EXPECT_EQ(Underlaying(MaskType::MASK_1) | Underlaying(MaskType::MASK_5), Underlaying(mask.getRawMask()));
}

TEST(TestMask, TestOperators)
{
	EXPECT_TRUE(nox::util::Mask<MaskType>(MaskType::MASK_1) == nox::util::Mask<MaskType>(MaskType::MASK_1));
	EXPECT_FALSE(nox::util::Mask<MaskType>(MaskType::MASK_1) == nox::util::Mask<MaskType>(MaskType::MASK_2));

	nox::util::Mask<MaskType> mask;
	EXPECT_TRUE(mask == MaskType::NONE);
	EXPECT_FALSE(mask & MaskType::MASK_1);
	EXPECT_FALSE(mask & MaskType::MASK_2);
	EXPECT_FALSE(mask & MaskType::MASK_3);
	EXPECT_FALSE(mask == MaskType::MASK_3);

	mask = { MaskType::MASK_1, MaskType::MASK_2 };
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_1));
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_2));
	EXPECT_FALSE(mask & MaskType::MASK_3);

	mask = nox::util::Mask<MaskType>(MaskType::MASK_1) | nox::util::Mask<MaskType>(MaskType::MASK_2);
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_1));
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_2));
	EXPECT_FALSE(mask & MaskType::MASK_3);

	mask = MaskType::NONE;
	mask |= MaskType::MASK_1;
	mask |= MaskType::MASK_2;
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_1));
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_2));
	EXPECT_FALSE(mask & MaskType::MASK_3);

	mask = (nox::util::Mask<MaskType>(MaskType::MASK_1) | nox::util::Mask<MaskType>(MaskType::MASK_2)) & nox::util::Mask<MaskType>(MaskType::MASK_2);
	EXPECT_FALSE(mask & MaskType::MASK_1);
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_2));
	EXPECT_FALSE(mask & MaskType::MASK_3);

	mask = nox::util::Mask<MaskType>(MaskType::MASK_1) | nox::util::Mask<MaskType>(MaskType::MASK_2);
	mask &= MaskType::MASK_1;
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_1));
	EXPECT_FALSE(mask & MaskType::MASK_2);
	EXPECT_FALSE(mask & MaskType::MASK_3);

	mask = nox::util::Mask<MaskType>(MaskType::MASK_1) | nox::util::Mask<MaskType>(MaskType::MASK_2);
	EXPECT_TRUE(static_cast<bool>(mask ^ MaskType::MASK_1));
	EXPECT_TRUE(static_cast<bool>(mask ^ MaskType::MASK_2));
	EXPECT_TRUE(static_cast<bool>(mask ^ MaskType::MASK_3));
	EXPECT_FALSE(mask ^ (nox::util::Mask<MaskType>(MaskType::MASK_1) |  nox::util::Mask<MaskType>(MaskType::MASK_2)));

	mask = nox::util::Mask<MaskType>(MaskType::MASK_1) | nox::util::Mask<MaskType>(MaskType::MASK_2);
	mask ^= MaskType::MASK_1;
	EXPECT_TRUE(static_cast<bool>(mask & MaskType::MASK_2));
	EXPECT_FALSE(mask & MaskType::MASK_1);
}

TEST(TestMask, TestFunctions)
{
	nox::util::Mask<MaskType> mask;

	mask.add(MaskType::MASK_1);
	EXPECT_TRUE(mask.matches(MaskType::MASK_1));
	EXPECT_FALSE(mask.matches(MaskType::MASK_2));
	EXPECT_FALSE(mask.matches(MaskType::MASK_3));

	mask.add(MaskType::MASK_3);
	EXPECT_TRUE(mask.matches(MaskType::MASK_1));
	EXPECT_FALSE(mask.matches(MaskType::MASK_2));
	EXPECT_TRUE(mask.matches(MaskType::MASK_3));

	mask.remove(MaskType::MASK_1);
	EXPECT_FALSE(mask.matches(MaskType::MASK_1));
	EXPECT_FALSE(mask.matches(MaskType::MASK_2));
	EXPECT_TRUE(mask.matches(MaskType::MASK_3));

	mask.add(MaskType::MASK_2);
	EXPECT_FALSE(mask.matches(MaskType::MASK_1));
	EXPECT_TRUE(mask.matches(MaskType::MASK_2));
	EXPECT_TRUE(mask.matches(MaskType::MASK_3));

	mask = mask.invert();
	EXPECT_TRUE(mask.matches(MaskType::MASK_1));
	EXPECT_FALSE(mask.matches(MaskType::MASK_2));
	EXPECT_FALSE(mask.matches(MaskType::MASK_3));
}
