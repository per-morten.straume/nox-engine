/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <nox/logic/Logic.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/world/Manager.h>

#include <memory>
#include <string>

class TestLogicContext: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		this->logic = std::make_unique<nox::logic::Logic>();
		this->context = this->logic.get();
	}

	virtual void TearDown()
	{
		this->context = nullptr;
		this->logic.reset();
	}

	std::unique_ptr<nox::logic::Logic> logic;
	nox::logic::IContext* context;
};

TEST_F(TestLogicContext, GetPhysics)
{
	ASSERT_TRUE(this->context->getPhysics() == nullptr);

	auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(this->context);
	const auto physicsPtr = physics.get();
	this->logic->setPhysics(std::move(physics));

	ASSERT_TRUE(this->context->getPhysics() == physicsPtr);
}

TEST_F(TestLogicContext, GetEventBroadcaster)
{
	ASSERT_TRUE(this->context->getEventBroadcaster() != nullptr);
}

TEST_F(TestLogicContext, GetWorldManager)
{
	ASSERT_TRUE(this->context->getWorldManager() == nullptr);

	auto world = std::make_unique<nox::logic::world::Manager>(this->context);
	const auto worldPtr = world.get();
	this->logic->setWorldManager(std::move(world));

	ASSERT_TRUE(this->context->getWorldManager() == worldPtr);
}
