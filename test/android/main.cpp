#include <nox/app/SdlApplication.h>

int main(int argc, char* argv[])
{
	auto app = nox::app::SdlApplication("NoxTest", "SuttungDigital");

	if (app.init(argc, argv) == false)
	{
		return 1;
	}

	app.shutdown();

	return 0;
}
