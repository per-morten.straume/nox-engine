set(NOX_TEST_FOLDER "NOX/test")

# Currently not supported on android
if(NOT ANDROID)
	add_subdirectory(unittest)
	add_subdirectory(testbed)
endif()

# Android specific tests
if(ANDROID)
	add_subdirectory(android)
endif()
