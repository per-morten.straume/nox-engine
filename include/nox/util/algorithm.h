/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_ALGORITHM_H_
#define NOX_UTIL_ALGORITHM_H_

#include <algorithm>

namespace nox { namespace util
{

template <class Type>
bool compareArray(const Type* first, const Type* second, int length);

/**
 * Removes all elements equal to a value from a container without shifting the elements.
 *
 * Using std::remove and container.erase will shift all elements after the erased elements one to the left.
 * This can be a big performance hit on large containers. This method swaps the removed element with the end element
 * before erasing the end, resulting only in one swap and one destruction per removed element.
 *
 * @warning The relative order of the elements will not be kept.
 *
 * @param container Container to remove from.
 * @param value Value to removed from container.
 * @return If any elements were removed or not.
 */
template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename ValueType
>
bool removeFast(Container<ContainedType, ContainerAllocator>& container, const ValueType& value);

/**
 * Removes the first element equal to a value from a container without shifting the elements.
 *
 * Using std::remove and container.erase will shift all elements after the erased elements one to the left.
 * This can be a big performance hit on large containers. This method swaps the removed element with the end element
 * before erasing the end, resulting only in one swap and one destruction per removed element.
 *
 * @warning The relative order of the elements will not be kept.
 *
 * @param container Container to remove from.
 * @param value Value to removed from container.
 * @return If any elements were removed or not.
 */
template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename ValueType
>
bool removeFirstFast(Container<ContainedType, ContainerAllocator>& container, const ValueType& value);

/**
 * Removes all elements where predicate returns true, from a container without shifting the elements.
 *
 * Using std::remove_if and container.erase will shift all elements after the erased elements one to the left.
 * This can be a big performance hit on large containers. This method swaps the removed element with the end element
 * before erasing the end, resulting only in one swap and one destruction per removed element.
 *
 * @warning The relative order of the elements will not be kept.
 *
 * @tparam UnaryPredicate Must have the signature "bool (const ContainedType &a)".
 * @param container Container to remove from.
 * @param predicate Predicate that returns true for elements to remove.
 * @return If any elements were removed or not.
 */
template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename UnaryPredicate
>
bool removeFastIf(Container<ContainedType, ContainerAllocator>& container, const UnaryPredicate& predicate);

/**
 * Erases the element pointed to by it, from a container without shifting the elements.
 *
 * Using container.erase will shift all elements after the erased elements one to the left.
 * This can be a big performance hit on large containers. This method swaps the removed element with the last element
 * before erasing the end, resulting only in one swap and one destruction per removed element.
 *
 * The returned iterator is completely valid and can be used to continue iteration over a range.
 *
 * @warning The relative order of the elements will not be kept.
 *
 * @param container Container to remove from.
 * @param elementIt Iterator to element to remove. This iterator will become invalid after the erasure.
 * @return Iterator to the new element that replaced the previous. This will be at the same position as elementIt was. Notice
 *         that this may be an end iterator.
 */
template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename Iterator
>
Iterator eraseFast(Container<ContainedType, ContainerAllocator>& container, Iterator elementIt);

/**
 * Merge a container into another container.
 *
 * mergeTo will contain its original elements plus the elements from mergeFrom.
 *
 * @param mergeTo Vector to merge into.
 * @param mergeFrom Vector to merge from.
 */
template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container
>
void mergeInto(Container<ContainedType, ContainerAllocator>& mergeTo, const Container<ContainedType, ContainerAllocator>& mergeFrom);

/**
 * Merge unique elements from a container into another container.
 *
 * mergeTo will contain its original elements plus the unique elements from mergeFrom.
 * Opposed to mergeInto, this function will not merge items already contained in mergeTo.
 *
 * @param mergeTo Vector to merge into.
 * @param mergeFrom Vector to merge from.
 */
template<typename Container>
void mergeIntoUnique(Container& mergeTo, const Container& mergeFrom);

/**
 * Get an Iterator to the first element in container equal to value.
 *
 * This function wraps std::find with a container interface. Its
 * behaviour is and will always be exactly that of std::find.
 *
 * @param value Value to find in container.
 * @param container Container to look for value in.
 * @return Iterator to the found element or end(container) if element is not found.
 */
template<typename Container, typename Value>
decltype(auto) find(const Container& container, const Value& value);

/**
 * Check if a container contains a value.
 *
 * @param value Value to find in container.
 * @param container Container to look for value in.
 * @return true if value is in container or false if not.
 */
template<typename Container, typename Value>
bool contains(const Container& container, const Value& value);


template <class Type>
bool compareArray(const Type* first, const Type* second, int length)
{
	for (int i = 0; i < length; i++)
	{
		if (first[i] != second[i])
		{
			return false;
		}
	}
	return true;
}

template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename ValueType
>
bool removeFast(Container<ContainedType, ContainerAllocator>& container, const ValueType& value)
{
	const auto containerEndIt = container.end();

	auto currentEndIt = containerEndIt;
	auto matchIt = std::find(container.begin(), currentEndIt, value);

	while (matchIt != currentEndIt)
	{
		--currentEndIt;

		if (matchIt != currentEndIt)
		{
			using std::swap;

			swap(*matchIt, *currentEndIt);

			matchIt = std::find(matchIt, currentEndIt, value);
		}
	}

	const bool removed = (currentEndIt != containerEndIt);

	container.erase(currentEndIt, containerEndIt);

	return removed;
}


template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename ValueType
>
inline bool removeFirstFast(Container<ContainedType, ContainerAllocator>& container, const ValueType& value)
{
	if (container.empty() == true)
	{
		return false;
	}

	const auto containerEndIt = container.end();
	const auto lastIt = containerEndIt - 1;

	auto matchIt = std::find(container.begin(), containerEndIt, value);

	if (matchIt != containerEndIt)
	{
		if (matchIt != lastIt)
		{
			using std::swap;
			swap(*matchIt, *lastIt);
		}

		container.erase(lastIt);

		return true;
	}
	else
	{
		return false;
	}
}

template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename> class Container,
	typename UnaryPredicate
>
bool removeFastIf(Container<ContainedType, ContainerAllocator>& container, const UnaryPredicate& predicate)
{
	const auto containerEndIt = container.end();

	auto currentEndIt = containerEndIt;
	auto matchIt = std::find_if(container.begin(), currentEndIt, predicate);

	while (matchIt != currentEndIt)
	{
		--currentEndIt;

		if (matchIt != currentEndIt)
		{
			using std::swap;

			swap(*matchIt, *currentEndIt);

			matchIt = std::find_if(matchIt, currentEndIt, predicate);
		}
	}

	const bool removed = (currentEndIt != containerEndIt);

	container.erase(currentEndIt, containerEndIt);

	return removed;
}

template
<
	typename ContainedType,
	typename ContainerAllocator,
	template<typename, typename > class Container,
	typename Iterator
>
Iterator eraseFast(Container<ContainedType, ContainerAllocator>& container, Iterator it)
{
	if (container.empty() == false)
	{
		const auto last = container.end() - 1;

		if (it == last)
		{
			container.erase(it);
			return container.end();
		}
		else
		{
			const auto itPosition = it - container.begin();

			using std::swap;
			swap(*it, *last);

			container.erase(last);
			return container.begin() + itPosition;
		}
	}
	else
	{
		return container.end();
	}
}

template<typename ContainedType, typename ContainerAllocator, template<typename, typename > class Container>
inline void mergeInto(Container<ContainedType, ContainerAllocator>& mergeTo, const Container<ContainedType, ContainerAllocator>& mergeFrom)
{
	mergeTo.insert(mergeTo.end(), mergeFrom.begin(), mergeFrom.end());
}

template<typename Container>
void mergeIntoUnique(Container& mergeTo, const Container& mergeFrom)
{
	for (const auto& item : mergeFrom)
	{
		if (!contains(mergeTo, item))
		{
			mergeTo.push_back(item);
		}
	}
}

template<typename Container, typename Value>
decltype(auto) find(const Container& container, const Value& value)
{
	using std::begin;
	using std::end;

	const auto first = begin(container);
	const auto last = end(container);
	return std::find(first, last, value);
}

template<typename Container, typename Value>
bool contains(const Container& container, const Value& value)
{
	using std::begin;
	using std::end;

	const auto first = begin(container);
	const auto last = end(container);
	return std::find(first, last, value) != last;
}

} }

#endif
