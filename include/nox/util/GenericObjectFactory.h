/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_UTIL_GENERICOBJECTFACTORY_H_
#define NOX_UTIL_GENERICOBJECTFACTORY_H_

#include <unordered_map>
#include <memory>

namespace nox
{
namespace util
{

/**
 * Create an instance of DerivedClass returned as a pointer to SuperClass.
 *
 * @tparam SuperClass The super/base/parent class type the pointer will have.
 * @tparam DerivedClass The derived/sub/child class type the new instance will be.
 * @tparam ConstructorArgs Argument types to pass to the constructor of DerivedClass.
 * @return SuperClass pointer to a new DerivedClass.
 */
template<class SuperClass, class DerivedClass, typename... ConstructorArgs>
std::unique_ptr<SuperClass> createDerivedObject(ConstructorArgs&&... args)
{
	return std::unique_ptr<SuperClass>(new DerivedClass(std::forward<ConstructorArgs>(args)...));
}

/**
 * A factory used to create classes from an ID.
 *
 * @tparam IdType The type of the ID used to map to class creations.
 * @tparam SuperClass The super/base/parent class type the pointer returned when creating an instance will have.
 * @tparam ConstructorArgs Argument types to pass to the constructor of SuperClass.
 */
template<class IdType, class SuperClass, typename... ConstructorArgs>
class GenericObjectFactory
{
public:
	/**
	 * Add a object creation mapping to the factory.
	 * An instance of the object can then be created by calling createObject()
	 * with the ID specified as parameter here.
	 *
	 * @tparam DerivedClass The derived class to create an instance of.
	 * @param id The id used to map to this object creation.
	 * @return true if it was added, false if it already existed.
	 */
	template<class DerivedClass>
	bool addObjectCreation(const IdType& id);

	/**
	 * Create an object mapped from the ID specified.
	 * The returned object will be of the type mapped from the ID. This mapping
	 * is created with addObjectCreation().
	 *
	 * @param id The id used to map to the object to create.
	 * @param args Arguments to pass to object constructor.
	 * @return Pointer of super class type to the derived class mapped from the ID, or nullptr if not mapped.
	 */
	std::unique_ptr<SuperClass> createObject(const IdType& id, ConstructorArgs&&... args) const;

private:
	typedef std::unique_ptr<SuperClass> (*CreateObjectFunction)(ConstructorArgs&&...);
	std::unordered_map<IdType, CreateObjectFunction> objectCreationFunctionMap;
};

template<class IdType, class SuperClass, typename... ConstructorArgs>
template<class DerivedClass>
bool GenericObjectFactory<IdType, SuperClass, ConstructorArgs...>::addObjectCreation(const IdType& id)
{
	if (this->objectCreationFunctionMap.find(id) != this->objectCreationFunctionMap.end())
	{
		return false;
	}

	this->objectCreationFunctionMap[id] = &createDerivedObject<SuperClass, DerivedClass, ConstructorArgs...>;
	return true;
}

template<class IdType, class SuperClass, typename... ConstructorArgs>
std::unique_ptr<SuperClass> GenericObjectFactory<IdType, SuperClass, ConstructorArgs...>::createObject(const IdType& id, ConstructorArgs&&... args) const
{
	auto mapIt = this->objectCreationFunctionMap.find(id);
	if (mapIt == this->objectCreationFunctionMap.end())
	{
		return nullptr;
	}

	CreateObjectFunction createObject = mapIt->second;
	return createObject(std::forward<ConstructorArgs>(args)...);
}

}
}

#endif
