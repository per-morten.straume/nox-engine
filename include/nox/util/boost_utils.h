/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file boost_utils.h
 * @brief Types to use with boost.
 */

#ifndef NOX_UTIL_BOOSTUTILS_H_
#define NOX_UTIL_BOOSTUTILS_H_

#include <glm/vec2.hpp>
#include <boost/geometry/core/cs.hpp>
#include <boost/geometry/geometries/register/point.hpp>
#include <boost/geometry/geometries/polygon.hpp>
#include <boost/geometry/geometries/linestring.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/segment.hpp>
#include <boost/geometry/multi/geometries/multi_polygon.hpp>

// This makes it so that we can use glm::vec2 with boost::geometry.
BOOST_GEOMETRY_REGISTER_POINT_2D(glm::vec2, glm::vec2::value_type, boost::geometry::cs::cartesian, x, y)

namespace nox { namespace util
{

using BoostRing = boost::geometry::model::ring<glm::vec2, false, true>;
using BoostPolygon = boost::geometry::model::polygon<glm::vec2, false, true>;
using BoostMultiPolygon = boost::geometry::model::multi_polygon<BoostPolygon>;
using BoostBox = boost::geometry::model::box<glm::vec2>;
using BoostSegment = boost::geometry::model::segment<glm::vec2>;
using BoostLineString = boost::geometry::model::linestring<glm::vec2>;

/**
 * Check if a boost ring is convex.
 * Ring must be closed.
 * Ring must be counter clockwise.
 * @param polygonRing Ring to check.
 * @return If convex or not.
 */
bool isConvex(const BoostPolygon::ring_type& polygonRing);

/**
 * Transform a polygon according to the parameters.
 * @return The transformed polygon.
 */
BoostPolygon transformPolygon(const BoostPolygon& polygon, const glm::vec2& translation, float rotation, const glm::vec2& scale);

BoostMultiPolygon transformPolygons(const BoostMultiPolygon& polygons, const glm::vec2& translation, float rotation, const glm::vec2& scale);

BoostBox transformBox(const BoostBox& polygon, const glm::vec2& translation, float rotation, const glm::vec2& scale);

/**
 * Remove points that are close to each other from a ring.
 * @param inputRing Ring to remove close points from.
 * @param[out] outputRing Ring with close points removed.
 * @param minPointDistance Minimum distance between points for them to exists. Points closer than this are merged.
 */
void removeClosePoints(const BoostRing& inputRing, BoostRing& outputRing, const float minPointDistance);

/**
 * Remove points that are close to each other from a polygon.
 * @param inputPolygon Polygon to remove close points from.
 * @param[out] outputPolygon Polygon with close points removed.
 * @param minPointDistance Minimum distance between points for them to exists. Points closer than this are merged.
 */
void removeClosePoints(const BoostPolygon& inputPolygon, BoostPolygon& outputPolygon, const float minPointDistance);

/**
 * Remove points that are close to each other from a polygon.
 * @param polygon Polygon to remove close points from. The points are removed directly from this polygon.
 * @param minPointDistance Minimum distance between points for them to exists. Points closer than this are merged.
 */
void removeClosePoints(BoostPolygon& polygon, const float minPointDistance);

} }

#endif
