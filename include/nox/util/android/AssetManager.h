/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_ANDROID_ASSETMANAGER_H_
#define NOX_ANDROID_ASSETMANAGER_H_

#include "AssetFile.h"

#include <jni.h>
#include <memory>
#include <string>
#include <vector>

namespace nox { namespace android {

class AssetManager
{
public:
	AssetManager();
	AssetManager(JNIEnv* javaEnv, jobject javaAssetManager);

	AssetManager(AssetManager&& other);
	AssetManager& operator=(AssetManager&& other);

	~AssetManager();

	std::vector<std::string> list(const std::string& path);
	AssetFile open(const std::string& path);

private:
	class Impl;
	std::unique_ptr<Impl> pimpl;
};

} }

#endif
