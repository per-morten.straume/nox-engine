/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_ANGLE_H_
#define NOX_MATH_ANGLE_H_

#include <glm/gtc/constants.hpp>

namespace nox { namespace math
{

/**
 * Clamp angle between (-pi, pi]
 */
float clampAngle(float angle);

/**
 * Calculate difference between to angles.
 *
 * Difference angle is clamped by clampAngle(float).
 */
float calculateAngleDifference(float firstAngle, float secondAngle);

/**
 * Clamp angle to a positive value. (e.g. -pi will be pi).
 */
float clampAnglePositive(float angle);

/**
 * Check if an angle is within a cone.
 * @param angle Angle to check.
 * @param coneMinAngle The start of the cone.
 * @param coneMaxAngle Then end of the cone.
 * @return true if within cone, otherwise false.
 */
bool angleIsWithinCone(float angle, float coneMinAngle, float coneMaxAngle);

/**
 * Radians for a full circle.
 * Equals 2 * pi
 * @tparam Type Type of value to return.
 */
template<typename Type>
Type fullCircle();

/**
 * Radians for a half circle.
 * Equals pi
 * @tparam Type Type of value to return.
 */
template<typename Type>
Type halfCircle();

/**
 * Radians for a quarter circle.
 * Equals pi / 2.0f
 * @tparam Type Type of value to return.
 */
template<typename Type>
Type quarterCircle();



template<typename Type>
inline Type fullCircle()
{
	return glm::pi<Type>() * Type(2.0);
}

template<typename Type>
inline Type halfCircle()
{
	return glm::pi<Type>();
}

template<typename Type>
inline Type quarterCircle()
{
	return glm::half_pi<Type>();
}

} }

#endif
