/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file json_utils.h
 * @brief Utility functions to work with json files/values.
 */

#ifndef NOX_UTIL_JSONUTILS_H_
#define NOX_UTIL_JSONUTILS_H_

#include <limits>
#include <json/value.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

namespace nox { namespace util
{

/**
 * How to name each coordinate when writing a vector.
 */
enum class JsonVecCoordType
{
	VERTEX, //!< x, y, z, w
	COLOR, //!< r, g, b, a
	TEXTURE //!< s, t, p, q
};

template<typename NumericType>
NumericType numericFromJson(const Json::Value& value);

/**
 * Parse a vec from a json value.
 *
 * The json value can be a single numeric, an object or an array.
 *
 * A single numeric will set all components to the same value.
 *
 * An object or array will set each component to their corresponding value.
 * There can be between 1 and four components depending on the function used (vec1, vec2, vec3, or vec4).
 *
 * In an object, each component has a name and a value:
 * - The first component can be named x, r or s.
 * - The second component can be named y, g or t.
 * - The third component can be named z, b or p.
 * - The fourth component can be named w, a or q.
 *
 * Example object: {"r": 1, "g": 0, "b": 0, "a": 0.5}
 *
 * In an array, each index has a value.
 *
 * Example array: [1, 0, 0, 0.5].
 *
 * @param jsonVec The json value containing the vec.
 * @param defaultValue The default value to return if parsing fails.
 * @return Returns the parsed value, or defaultValue if parsing failed.
 */
template<typename T, glm::precision P>
glm::tvec1<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec1<T, P>& defaultValue);

/**
 * @see parseJsonVec
 */
template<typename T, glm::precision P>
glm::tvec2<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec2<T, P>& defaultValue);

/**
 * @see parseJsonVec
 */
template<typename T, glm::precision P>
glm::tvec3<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec3<T, P>& defaultValue);

/**
 * @see parseJsonVec
 */
template<typename T, glm::precision P>
glm::tvec4<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec4<T, P>& defaultValue);

/**
 * Write a vec to a json value.
 *
 * The json value will be written as a single numeric if all elements of vec are equal. Otherwise
 * it will be written as an object with one member per element.
 *
 * @note To read the value written, use parseJsonVec().
 */
template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec1<T, P>& vec);

/**
 * @see writeJsonVec(const glm::tvec1<T, P>&)
 *
 * @param coordType Decides how the vec should be written. @see JsonVecCoordType.
 */
template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec2<T, P>& vec, const JsonVecCoordType coordType = JsonVecCoordType::VERTEX);

/**
 * @see writeJsonVec(const glm::tvec2<T, P>&)
 */
template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec3<T, P>& vec, const JsonVecCoordType coordType = JsonVecCoordType::VERTEX);

/**
 * @see writeJsonVec(const glm::tvec2<T, P>&)
 */
template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec4<T, P>& vec, const JsonVecCoordType coordType = JsonVecCoordType::VERTEX);


template<typename NumericType>
inline NumericType numericFromJson(const Json::Value& value)
{
	static_assert(std::is_arithmetic<NumericType>::value, "NumericType needs to be an arithmetic type (floating point or integer).");

	if (std::is_integral<NumericType>::value == true)
	{
		return static_cast<NumericType>(value.asLargestInt());
	}
	else if (std::is_floating_point<NumericType>::value == true)
	{
		return static_cast<NumericType>(value.asDouble());
	}
}

template<typename T, glm::precision P>
glm::tvec1<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec1<T, P>& defaultValue)
{
	using VecType = glm::tvec1<T, P>;
	using ValueType = typename VecType::value_type;

	VecType vector = defaultValue;

	if (jsonVec.isNumeric() == true)
	{
		vector = VecType(numericFromJson<ValueType>(jsonVec));
	}
	else if (jsonVec.isObject() == true)
	{
		auto value = jsonVec["x"];

		if (value.isNull() == true)
		{
			value = jsonVec["r"];
		}

		if (value.isNull() == true)
		{
			value = jsonVec["s"];
		}

		if (value.isNull() == false && value.isNumeric() == true)
		{
			vector.x = numericFromJson<ValueType>(value);
		}
	}
	else if (jsonVec.isArray() == true)
	{
		vector.x = numericFromJson<ValueType>(jsonVec[0]);
	}

	return vector;
}

template<typename T, glm::precision P>
glm::tvec2<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec2<T, P>& defaultValue)
{
	using VecType = glm::tvec2<T, P>;
	using ValueType = typename VecType::value_type;

	auto vector = VecType{
		parseJsonVec(jsonVec, glm::tvec1<T, P>(defaultValue)).x,
		defaultValue.y
	};

	if (jsonVec.isNumeric() == true)
	{
		vector = VecType(numericFromJson<ValueType>(jsonVec));
	}
	else if (jsonVec.isObject() == true)
	{
		auto value = jsonVec["y"];

		if (value.isNull() == true)
		{
			value = jsonVec["g"];
		}

		if (value.isNull() == true)
		{
			value = jsonVec["t"];
		}

		if (value.isNull() == false && value.isNumeric() == true)
		{
			vector.y = numericFromJson<ValueType>(value);
		}
	}
	else if (jsonVec.isArray() == true)
	{
		vector.y = numericFromJson<ValueType>(jsonVec[1]);
	}

	return vector;
}

template<typename T, glm::precision P>
glm::tvec3<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec3<T, P>& defaultValue)
{
	using VecType = glm::tvec3<T, P>;
	using ValueType = typename VecType::value_type;

	auto vector = VecType{
		parseJsonVec(jsonVec, glm::tvec2<T, P>(defaultValue)),
		defaultValue.z
	};

	if (jsonVec.isNumeric() == true)
	{
		vector = VecType(numericFromJson<ValueType>(jsonVec));
	}
	else if (jsonVec.isObject() == true)
	{
		auto value = jsonVec["z"];

		if (value.isNull() == true)
		{
			value = jsonVec["b"];
		}

		if (value.isNull() == true)
		{
			value = jsonVec["p"];
		}

		if (value.isNull() == false && value.isNumeric() == true)
		{
			vector.z = numericFromJson<ValueType>(value);
		}
	}
	else if (jsonVec.isArray() == true)
	{
		vector.z = numericFromJson<ValueType>(jsonVec[2]);
	}

	return vector;
}

template<typename T, glm::precision P>
glm::tvec4<T, P> parseJsonVec(const Json::Value& jsonVec, const glm::tvec4<T, P>& defaultValue)
{
	using VecType = glm::tvec4<T, P>;
	using ValueType = typename VecType::value_type;

	auto vector = VecType{
		parseJsonVec(jsonVec, glm::tvec3<T, P>(defaultValue)),
		defaultValue.w
	};

	if (jsonVec.isNumeric() == true)
	{
		vector = VecType(numericFromJson<ValueType>(jsonVec));
	}
	else if (jsonVec.isObject() == true)
	{
		auto value = jsonVec["w"];

		if (value.isNull() == true)
		{
			value = jsonVec["a"];
		}

		if (value.isNull() == true)
		{
			value = jsonVec["q"];
		}

		if (value.isNull() == false && value.isNumeric() == true)
		{
			vector.w = numericFromJson<ValueType>(value);
		}
	}
	else if (jsonVec.isArray() == true)
	{
		vector.w = numericFromJson<ValueType>(jsonVec[3]);
	}

	return vector;
}

template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec1<T, P>& vec)
{
	return Json::Value(vec.x);
}

template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec2<T, P>& vec, const JsonVecCoordType coordType)
{
	if (vec.x == vec.y)
	{
		return Json::Value(vec.x);
	}
	else
	{
		Json::Value vecObject;

		if (coordType == JsonVecCoordType::VERTEX)
		{
			vecObject["x"] = vec.x;
			vecObject["y"] = vec.y;
		}
		else if (coordType == JsonVecCoordType::COLOR)
		{
			vecObject["r"] = vec.r;
			vecObject["g"] = vec.g;
		}
		else if (coordType == JsonVecCoordType::TEXTURE)
		{
			vecObject["s"] = vec.s;
			vecObject["t"] = vec.t;
		}

		return vecObject;
	}
}

template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec3<T, P>& vec, const JsonVecCoordType coordType)
{
	if (vec.x == vec.y && vec.x == vec.z)
	{
		return Json::Value(vec.x);
	}
	else
	{
		Json::Value vecObject = writeJsonVec(glm::tvec2<T, P>(vec));

		if (coordType == JsonVecCoordType::VERTEX)
		{
			vecObject["z"] = vec.z;
		}
		else if (coordType == JsonVecCoordType::COLOR)
		{
			vecObject["b"] = vec.b;
		}
		else if (coordType == JsonVecCoordType::TEXTURE)
		{
			vecObject["p"] = vec.p;
		}

		return vecObject;
	}
}

template<typename T, glm::precision P>
Json::Value writeJsonVec(const glm::tvec4<T, P>& vec, const JsonVecCoordType coordType)
{
	if (vec.x == vec.y && vec.x == vec.z && vec.x == vec.w)
	{
		return Json::Value(vec.x);
	}
	else
	{
		Json::Value vecObject = writeJsonVec(glm::tvec3<T, P>(vec));

		if (coordType == JsonVecCoordType::VERTEX)
		{
			vecObject["w"] = vec.w;
		}
		else if (coordType == JsonVecCoordType::COLOR)
		{
			vecObject["a"] = vec.a;
		}
		else if (coordType == JsonVecCoordType::TEXTURE)
		{
			vecObject["q"] = vec.q;
		}

		return vecObject;
	}
}

}
}

#endif
