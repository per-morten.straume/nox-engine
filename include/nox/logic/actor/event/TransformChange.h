/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_TRANSFORMCHANGE_H_
#define NOX_LOGIC_ACTOR_TRANSFORMCHANGE_H_

#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <nox/logic/actor/event/Event.h>

namespace nox { namespace logic { namespace actor
{

class Actor;

/**
 * The transformation of an actor changed.
 */
class TransformChange final: public Event
{
public:
	static const event::Event::IdType ID;

	TransformChange(Actor* transformedActor, const glm::vec2& position, float rotation, const glm::vec2& scale);
	~TransformChange();

	glm::mat4 getTransformMatrix() const;
	const glm::vec2& getPosition() const;
	const glm::vec2& getScale() const;
	float getRotation() const;

private:
	glm::vec2 position;
	glm::vec2 scale;
	float rotation;
};

} } }

#endif
