/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_STORAGE_MEMORYDATASTORAGE_H_
#define NOX_APP_STORAGE_MEMORYDATASTORAGE_H_

#include "IDataStorage.h"

namespace nox { namespace app { namespace storage {

/**
 * Stores files only in memory.
 *
 * This storage does NOT save any files to any kind of persistent
 * storage (e.g filesystem). The files are only kept in memory and
 * are lost forever when the storage is destroyed. It should only
 * be used for testing purposes where a persistent storage is not available.
 */
class MemoryDataStorage final: public IDataStorage
{
public:
	MemoryDataStorage();
	~MemoryDataStorage();

	bool fileExists(const std::string& filePath) const override;
	std::unique_ptr<std::istream> openReadableFile(const std::string& filePath) override;
	std::unique_ptr<std::ostream> openWritableFile(const std::string& filePath, bool append = false) override;
	std::string readFileContent(const std::string& filePath) override;
	void writeFileContent(const std::string& filePath, const std::string& content, bool append = false) override;
	void removeFile(const std::string& filePath) override;

private:
	class Impl;
	std::unique_ptr<Impl> d;
};

} } }

#endif
