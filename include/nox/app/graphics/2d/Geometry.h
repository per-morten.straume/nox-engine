/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GEOMETRY_H_
#define NOX_APP_GRAPHICS_GEOMETRY_H_

#include <nox/app/graphics/GlVertexAttribute.h>

#include <vector>
#include <glm/glm.hpp>

namespace nox { namespace app { namespace graphics
{

class GeometrySet;

enum class GeometryPrimitive
{
	TRIANGLE,
	LINE
};

enum class GeometryType
{
	TRIANGLE,
	QUAD,
	LINE,
	LINELOOP,
	POLYGON
};

struct ObjectCoordinate
{
public:
	ObjectCoordinate() = default;

	ObjectCoordinate(const glm::vec2& geometryVertex, const glm::vec4& color) :
		geometryVertex(geometryVertex),
		color(color)
	{}

	static GlPackedVertexAttributeDef getVertexAttributeDefs(const GlVertexAttributeLocation positionLocation, const GlVertexAttributeLocation colorLocation);

	glm::vec2 geometryVertex;
	glm::vec4 color;
};

/**
 * Representation of geometry.
 * Several classes inherit from this to represent some form of geometry.
 * Every time the color or vertex of a geometry instance is change
 * you need to call signalChange() to update the actual renderable
 * vertices.
 */
class Geometry
{
public:
	/**
	 * Construct Geometry.
	 * @param container The container which this geometry is constructed from.
	 */
	Geometry(GeometrySet* container);

	virtual ~Geometry();

	/**
	 * Set the color for the whole object (all vertices).
	 * @param color The color to set.
	 */
	virtual void setColor(const glm::vec4& color) = 0;

	/**
	 * Generates vertices ready for rendering.
	 * @return The vertices generated.
	 */
	virtual std::vector<ObjectCoordinate> generateRenderableVertices() const = 0;

	/**
	 * The number of vertices used for rendering.
	 * This number will be the same as the size of the returned vector of generateRenderableVertices().
	 * @return Number of vertices.
	 */
	virtual unsigned int getNumVertices() const = 0;

	/**
	 * Get the number of render object.
	 * A shape (geometry) might consist of several shapes. E.g. a quad
	 * has two triangles.
	 * This is information used by the renderer.
	 * @return Number of render objects.
	 */
	virtual unsigned int getNumRenderObjects() const = 0;

	virtual GeometryType getType() const = 0;

	virtual GeometryPrimitive getPrimitiveType() const = 0;

	/**
	 * Signal that the vertices has changed to the GeometrySet that contains
	 * this geometry.
	 * When changing colors or vertices, you must call this function for the
	 * representation in the containing GeometrySet to be correct.
	 * This will update the GeometrySet's representation of the vertices.
	 * This function is expensive since the renderable vertices has to
	 * be recalculated and copied.
	 */
	void signalChange();

private:
	GeometrySet* container;
};


class Triangle: public Geometry
{
public:
	Triangle(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;
	GeometryType getType() const override;
	GeometryPrimitive getPrimitiveType() const override;

	void setVertexA(const glm::vec2& vertex);
	void setVertexB(const glm::vec2& vertex);
	void setVertexC(const glm::vec2& vertex);
	void setVertices(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c);
	void setAsIsosceles(const glm::vec2& position, const float angle, const float width, const float length);

	glm::vec2 getVertexA() const;
	glm::vec2 getVertexB() const;
	glm::vec2 getVertexC() const;
	
	void setColorA(const glm::vec4& color);
	void setColorB(const glm::vec4& color);
	void setColorC(const glm::vec4& color);
	void setColors(const glm::vec4& a, const glm::vec4& b, const glm::vec4& c);

private:
	ObjectCoordinate a;
	ObjectCoordinate b;
	ObjectCoordinate c;
};


/**
 * Polygon with four squares.
 * This is the same as a Polygon with only four vertices,
 * but this might be more efficient since it stores
 * only two triangles for the rendering, rather than four.
 */
class Quad: public Geometry
{
public:
	Quad(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;
	GeometryType getType() const override;
	GeometryPrimitive getPrimitiveType() const override;

	void setVertexBottomLeft(const glm::vec2& vertex);
	void setVertexBottomRight(const glm::vec2& vertex);
	void setVertexTopRight(const glm::vec2& vertex);
	void setVertexTopLeft(const glm::vec2& vertex);
	void setVertices(const glm::vec2& bottomLeft, const glm::vec2& bottomRight, const glm::vec2& topRight, const glm::vec2& topLeft);

	void setColorBottomLeft(const glm::vec4& color);
	void setColorBottomRight(const glm::vec4& color);
	void setColorTopRight(const glm::vec4& color);
	void setColorTopLeft(const glm::vec4& color);
	void setColors(const glm::vec4& bottomLeft, const glm::vec4& bottomRight, const glm::vec4& topRight, const glm::vec4& topLeft);

private:
	ObjectCoordinate bottomLeft;
	ObjectCoordinate bottomRight;
	ObjectCoordinate topRight;
	ObjectCoordinate topLeft;
};


class Line: public Geometry
{
public:
	Line(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;
	GeometryType getType() const override;
	GeometryPrimitive getPrimitiveType() const override;

	const glm::vec2& getLineBegin() const;
	const glm::vec2& getLineEnd() const;

	void setLineBegin(const glm::vec2& vertex);
	void setLineEnd(const glm::vec2& vertex);
	void setVertices(const glm::vec2& start, const glm::vec2& end);

	void setColorStart(const glm::vec4& color);
	void setColorEnd(const glm::vec4& color);
	void setColors(const glm::vec4& start, const glm::vec4& end);

private:
	ObjectCoordinate start;
	ObjectCoordinate end;
};


class LineLoop: public Geometry
{
public:
	LineLoop(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;
	GeometryType getType() const override;
	GeometryPrimitive getPrimitiveType() const override;

	void init(unsigned int numVertices, bool closed);
	void setVertex(unsigned int vertexNum, const glm::vec2& vertex);
	void setColor(unsigned int vertexNum, const glm::vec4& color);

private:
	std::vector<ObjectCoordinate> coordinates;
	glm::vec4 color;
	unsigned int numVertices;
	unsigned int numLines;
	bool closed;
};


//TODO: Use indices for a triangle fan.
/**
 * Polygon.
 * This can be both convex and concave.
 */
class Polygon: public Geometry
{
public:
	Polygon(GeometrySet* container);

	void setColor(const glm::vec4& color) override;
	std::vector<ObjectCoordinate> generateRenderableVertices() const override;
	unsigned int getNumVertices() const override;
	unsigned int getNumRenderObjects() const override;
	GeometryType getType() const override;
	GeometryPrimitive getPrimitiveType() const override;

	void init(unsigned int numVertices);
	void setVertex(unsigned int vertexNum, const glm::vec2& vertex);
	void setColor(unsigned int vertexNum, const glm::vec4& color);

private:
	std::vector<ObjectCoordinate> coordinates;
	glm::vec4 color;
	unsigned int numVertices;
	unsigned int numTriangles;
};

} } }

#endif
