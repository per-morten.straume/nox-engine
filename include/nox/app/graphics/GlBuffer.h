/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GLBUFFER_H_
#define NOX_APP_GRAPHICS_GLBUFFER_H_

#include <nox/app/graphics/GlContextState.h>
#include <nox/app/graphics/GlBlockingBufferUploader.h>
#include <nox/util/math/exponential.h>

#if NOX_OPENGL_SUPPORT_MAPBUFFER
#include <nox/app/graphics/GlAsyncBufferUploader.h>
#endif

#include <glm/glm.hpp>

namespace nox { namespace app { namespace graphics {

using GlDefaultBufferUploader = GlBlockingBufferUploader;

#if NOX_OPENGL_SUPPORT_MAPBUFFER
using GlMaybeAsyncBufferUploader = GlAsyncBufferUploader;
#else
using GlMaybeAsyncBufferUploader = GlBlockingBufferUploader;
#endif

class GlBuffer
{
public:
	GlBuffer();

	~GlBuffer();

	GlBuffer(GlBuffer&& other);
	GlBuffer& operator=(GlBuffer&& other);

	GlBuffer(const GlBuffer&) = delete;
	GlBuffer& operator=(const GlBuffer&) = delete;

	void bind(GlContextState* contextState);

	bool isValid() const;
	GLenum getType() const;
	GLenum getUsage() const;
	GLuint getName() const;

	template<typename T>
	void overwriteData(const std::vector<T>& data,
		GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void overwriteData(const std::vector<T>& data,
		const typename std::vector<T>::size_type begin,
		const typename std::vector<T>::size_type length,
		GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void overwriteSubData(const std::vector<T>& data, const std::size_t subBegin,
		GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void overwriteSubData(const std::vector<T>& data,
		const typename std::vector<T>::size_type dataBegin,
		const typename std::vector<T>::size_type length,
		const std::size_t subBegin,
		GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void resize(const typename std::vector<T>::size_type size,
		GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void clear(GlContextState* state,
		GlBufferUploader& uploader);

	template<typename T>
	void overwriteData(const std::vector<T>& data,
		GlContextState* state);

	template<typename T>
	void overwriteData(const std::vector<T>& data,
		const typename std::vector<T>::size_type begin,
		const typename std::vector<T>::size_type length,
		GlContextState* state);

	template<typename T>
	void overwriteSubData(const std::vector<T>& data, const std::size_t subBegin,
		GlContextState* state);

	template<typename T>
	void overwriteSubData(const std::vector<T>& data,
		const typename std::vector<T>::size_type dataBegin,
		const typename std::vector<T>::size_type length,
		const std::size_t subBegin,
		GlContextState* state);

	template<typename T>
	void resize(const typename std::vector<T>::size_type size,
		GlContextState* state);

	template<typename T>
	void clear(GlContextState* state);

	static GlBuffer generate(const GLenum type, const GLenum usage);
	static GlBuffer generateArrayBuffer(const GLenum usage);
	static GlBuffer generateElementArrayBuffer(const GLenum usage);

private:
	GlBuffer(const GLenum type, const GLenum usage, const GLuint name);
	GlBuffer(const GLenum type, const GLuint name);

	void ensureSufficientSpace(const std::size_t size, GlContextState* state, GlBufferUploader& uploader);
	void verifyValidity();

	GLenum type;
	GLenum usage;
	GLuint name;
	std::size_t size;
};

template<typename T>
void GlBuffer::overwriteData(const std::vector<T>& data,
	GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	const auto sizeBytes = data.size() * sizeof(T);
	this->ensureSufficientSpace(sizeBytes, state, uploader);
	uploader.uploadData(*this, reinterpret_cast<const void*>(data.data()), sizeBytes, state);
}

template<typename T>
void GlBuffer::overwriteData(const std::vector<T>& data,
	const typename std::vector<T>::size_type begin,
	const typename std::vector<T>::size_type length,
	GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	const auto sizeBytes = length * sizeof(T);
	this->ensureSufficientSpace(sizeBytes, state, uploader);
	uploader.uploadData(*this, reinterpret_cast<const void*>(&data[begin]), sizeBytes, state);
}

template<typename T>
void GlBuffer::overwriteSubData(const std::vector<T>& data, const std::size_t subBegin,
	GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	const auto sizeBytes = data.size() * sizeof(T);
	if (subBegin + sizeBytes > this->size)
	{
		throw std::logic_error("Tried uploading subdata with an out of bounds end.");
	}

	uploader.uploadSubData(*this, reinterpret_cast<const void*>(data.data()), sizeBytes, subBegin, state);
}

template<typename T>
void GlBuffer::overwriteSubData(const std::vector<T>& data,
	const typename std::vector<T>::size_type dataBegin,
	const typename std::vector<T>::size_type length,
	const std::size_t subBegin,
	GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	const auto sizeBytes = length * sizeof(T);
	if (subBegin + sizeBytes > this->size)
	{
		throw std::logic_error("Tried uploading subdata with an out of bounds end.");
	}

	uploader.uploadSubData(*this, reinterpret_cast<const void*>(&data[dataBegin]), sizeBytes, subBegin, state);
}

template<typename T>
void GlBuffer::resize(const typename std::vector<T>::size_type size,
	GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	this->size = size;

	uploader.resizeBuffer(*this, this->size * sizeof(T), state);
}

template<typename T>
void GlBuffer::clear(GlContextState* state,
	GlBufferUploader& uploader)
{
	this->verifyValidity();

	if (this->size > 0)
	{
		this->resize<T>(0, uploader, state);
	}
}

template<typename T>
void GlBuffer::overwriteData(const std::vector<T>& data,
	GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->overwriteData(data, state, uploader);
}

template<typename T>
void GlBuffer::overwriteData(const std::vector<T>& data,
	const typename std::vector<T>::size_type begin,
	const typename std::vector<T>::size_type length,
	GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->overwriteData(data, begin, length, state, uploader);
}

template<typename T>
void GlBuffer::overwriteSubData(const std::vector<T>& data, const std::size_t subBegin,
	GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->overwriteData(data, subBegin, state, uploader);
}

template<typename T>
void GlBuffer::overwriteSubData(const std::vector<T>& data,
	const typename std::vector<T>::size_type dataBegin,
	const typename std::vector<T>::size_type length,
	const std::size_t subBegin,
	GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->overwriteData(data, dataBegin, length, subBegin, state, uploader);
}

template<typename T>
void GlBuffer::resize(const typename std::vector<T>::size_type size,
	GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->resize(size, state, uploader);
}

template<typename T>
void GlBuffer::clear(GlContextState* state)
{
	auto uploader = GlDefaultBufferUploader();
	this->clear<T>(state, uploader);
}

} } }

#endif
