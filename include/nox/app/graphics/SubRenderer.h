/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_SUBRENDERER_H_
#define NOX_APP_GRAPHICS_SUBRENDERER_H_

#include <glm/vec2.hpp>

namespace nox { namespace app {

class IContext;

namespace graphics {

class RenderData;

/**
 * A sub renderer that can be attached to the Renderer to render a certain job.
 */
class SubRenderer
{
public:
	virtual ~SubRenderer()
	{}

	/**
	 * Initialize the renderer.
	 * @return if it was properly initialized or not.
	 */
	virtual bool init(IContext* context, RenderData& renderData, const glm::uvec2& screenSize) = 0;

	/**
	 * Always called once before render is called to prepare render data.
	 */
	virtual void prepareRendering(RenderData& renderData) = 0;

	/**
	 * Do the actual rendering.
	 */
	virtual void render(RenderData& renderData) = 0;

	/**
	 * Called when the size of the screen has changed.
	 * @param screenSize New size of the screen
	 */
	virtual void onScreenSizeChange(const glm::uvec2& screenSize);
};

}
} }

#endif
