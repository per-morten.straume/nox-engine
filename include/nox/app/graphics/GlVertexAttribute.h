/* NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_GLVERTEXATTRIBUTE_H_
#define NOX_APP_GRAPHICS_GLVERTEXATTRIBUTE_H_

#include <nox/app/graphics/GlContextState.h>
#include "GlDataType.h"
#include "GlBuffer.h"

#include <cstddef>
#include <cassert>

namespace nox { namespace app { namespace graphics {

class GlVertexAttributeLocation
{
public:
	struct Unused_t {};

	GlVertexAttributeLocation(const std::size_t index):
		index(index),
		used(true)
	{}

	GlVertexAttributeLocation(const Unused_t):
		index(0),
		used(false)
	{}

	bool isUsed() const;
	std::size_t getLocation() const;

private:
	std::size_t index;
	bool used;
};

struct GlVertexAttributeDef
{
	GlVertexAttributeDef(
			const GlVertexAttributeLocation& location,
			const std::size_t numComponents,
			const GlDataType componentType,
			const std::size_t offset,
			const bool normalized = false):
		location(location),
		numComponents(numComponents),
		offset(offset),
		componentType(componentType),
		normalized(normalized)
	{}

	GlVertexAttributeLocation location;
	std::size_t numComponents;
	std::size_t offset;
	GlDataType componentType;
	bool normalized;
};

struct GlPackedVertexAttributeDef
{
	explicit GlPackedVertexAttributeDef(
			const std::size_t packSize,
			const std::vector<GlVertexAttributeDef>& defList):
		packSize(packSize),
		defList(defList)
	{}

	std::size_t packSize;
	std::vector<GlVertexAttributeDef> defList;
};

class GlPackedVertexAttributes
{
public:
	GlPackedVertexAttributes(const GLenum bufferUsage, const GlPackedVertexAttributeDef& def);

	void bind(GlContextState* state);
	GlBuffer& getBuffer();

private:
	GlBuffer buffer;

	struct Attribute
	{
		GLuint index;
		GLint size;
		GLenum type;
		GLboolean normalized;
		const GLvoid* pointer;
	};

	GLsizei stride;
	std::vector<Attribute> attributes;
};

} } }

#endif
