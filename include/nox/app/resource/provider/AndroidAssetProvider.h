/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_ANDROIDASSETSPROVIDER_H_
#define NOX_APP_RESOURCE_ANDROIDASSETSPROVIDER_H_

#include <nox/app/resource/provider/Provider.h>
#include <nox/log/Logger.h>
#include <nox/common/api.h>
#include <nox/util/android/AssetManager.h>

#include <vector>
#include <map>

namespace nox { namespace app { namespace resource {

class NOX_API AndroidAssetProvider: public Provider
{
public:
	AndroidAssetProvider(JNIEnv* javaEnv, jobject javaAssetManager, std::string rootPath = "");

	bool open() override;
	std::vector<char> readRawResource(const Descriptor& descriptor) override;
	std::vector<File> listFiles(const File& directory) override;

	void setLogger(log::Logger log);

private:
	std::string removeRootSegment(const std::string& path) const;

	log::Logger log;
	std::string rootPath;
	android::AssetManager assetManager;
	std::map<std::string, std::vector<File>> directories;
};

} } }

#endif
