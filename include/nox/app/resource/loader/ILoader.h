/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_RESOURCE_ILOADER_H_
#define NOX_APP_RESOURCE_ILOADER_H_

#include <nox/common/api.h>
#include <nox/util/Buffer.h>

#include <string>
#include <memory>
#include <vector>

namespace nox { namespace app { namespace resource {

class IExtraData;

/**
 * Loads a resource from a raw buffer in memory.
 */
class NOX_API ILoader
{
public:
	virtual ~ILoader();

	/**
	 * Get the pattern for the file names that this resource loader wants to load.
	 * The pattern uses asterisk (*) for wildcard match, and question mark (?) for single character match.
	 */
	virtual const char* getPattern() const = 0;

	/**
	 * Check if the loaded resource uses the raw file buffer, or parses it and stores it in another format.
	 *
	 * When using a raw file, the resource data is accessed directly through the buffer (ResourceHandle::getBuffer()). Otherwise
	 * the data is accessed through ResourceHandle::getExtra().
	 *
	 * @return true if using raw file, otherwise false.
	 */
	virtual bool useRawFile() const = 0;

	/**
	 * Get the size of a loaded resource.
	 *
	 * For a resource using the raw file (useRawFile()), the size will match rawSize. For other resources
	 * the size will vary.
	 *
	 * @param rawBuffer Pointer to the buffer loaded.
	 * @param rawSize Size of the buffer loaded.
	 * @return Size of the resource loaded, in bytes.
	 */
	virtual unsigned int getLoadedResourceSize(const char* rawBuffer, const unsigned int rawSize) const = 0;

	/**
	 * Load a resource.
	 * @param inputBuffer Pointer to the data to load.
	 * @param[out] outputBuffer Filled with the loaded data.
	 * @param[out] extraData Newly allocated IExtraData with extra data specific to the resource.
	 * @return true if successfully loaded, otherwise false.
	 */
	virtual bool loadResource(const util::Buffer<char> inputBuffer, std::vector<char>& outputBuffer, std::unique_ptr<IExtraData>& extraData) const = 0;
};

} } }

#endif
