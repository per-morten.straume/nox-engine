/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/*
 * Derived from origianl work from this article: http://xissburg.com/faster-gaussian-blur-in-glsl/
 */

//? required 130
in vec2 fragTexCoord;
in vec2 blurTexCoords[14];

uniform sampler2D renderTexture;

out vec4 outputFragColor;


void main()
{
	outputFragColor = vec4(0.0);
	outputFragColor += texture(renderTexture, blurTexCoords[0]) * 0.0044299121055113265;
	outputFragColor += texture(renderTexture, blurTexCoords[1]) * 0.00895781211794;
	outputFragColor += texture(renderTexture, blurTexCoords[2]) * 0.0215963866053;
	outputFragColor += texture(renderTexture, blurTexCoords[3]) * 0.0443683338718;
	outputFragColor += texture(renderTexture, blurTexCoords[4]) * 0.0776744219933;
	outputFragColor += texture(renderTexture, blurTexCoords[5]) * 0.115876621105;
	outputFragColor += texture(renderTexture, blurTexCoords[6]) * 0.147308056121;
	outputFragColor += texture(renderTexture, fragTexCoord) * 0.159576912161;
	outputFragColor += texture(renderTexture, blurTexCoords[7]) * 0.147308056121;
	outputFragColor += texture(renderTexture, blurTexCoords[8]) * 0.115876621105;
	outputFragColor += texture(renderTexture, blurTexCoords[9]) * 0.0776744219933;
	outputFragColor += texture(renderTexture, blurTexCoords[10]) * 0.0443683338718;
	outputFragColor += texture(renderTexture, blurTexCoords[11]) * 0.0215963866053;
	outputFragColor += texture(renderTexture, blurTexCoords[12]) * 0.00895781211794;
	outputFragColor += texture(renderTexture, blurTexCoords[13]) * 0.0044299121055113265;
}